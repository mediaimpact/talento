$(document).ready(function(){

    if (window.location.hash == '#_=_'){
        if (history.replaceState) {
            var cleanHref = window.location.href.split('#')[0];
            history.replaceState(null, null, cleanHref);

        } else {
            window.location.hash = '';
        }
    }

   $("#perfil").validate({
  // Specify validation rules
    rules: {
      nombre: "required",
      apellido: "required",
      telefono: "required",
      email: "required",
      sueldo: "required",
      cv: "required",
    },
    // Specify validation error messages
    messages: {
      nombre:"Ingresar nombre",
      
      apellido: "ingresar apellido",
      
      telefono: "Ingresar teléfono",
      email: "Ingresar Email",
      
      sueldo: "Ingresar sus expectativas salariales",   

      cv: "Adjuntar su CV",  
    }
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
  });
  $("#enviarDatos").click(function(event) {
  /* Act on the event */
    $("#perfil").valid();
  });

   function mostrarImagen(input,variable) {
     if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
       $('#des_url1').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
     }
    }
     
    $("#file_url1").change(function(){
      var variable = $(this).attr('data-var');
     mostrarImagen(this,variable);
    });


  // $(".adjuntar-foto").click(function(){
  //   if ($("#modelo").valid()) {
      
  //     formData.append("file", $("#modelo")[0].files[0]);
  //   }
  // })
});