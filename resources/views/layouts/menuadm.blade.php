<ul class="nav flex-column">
    <li class="nav-item text-center">
        <a class="nav-link" id="amd1" href="{{route('puestos')}}" data-toggle="tooltip" data-placement="right" title="Historial puestos creados"><i class="fa fa-list-ol" aria-hidden="true"></i></a>
    </li>
    <li class="nav-item text-center">
        <a class="nav-link" id="amd2" href="{{route('add_puesto')}}" data-toggle="tooltip" data-placement="right" title="Crear nuevo puesto"><i class="fa fa-plus" aria-hidden="true"></i></a>
    </li>
    <li class="nav-item text-center">
        <a class="nav-link" id="amd3" href="{{route('postulantes_vacantes')}}" data-toggle="tooltip" data-placement="right" title="Lista de postulantes"><i class="fa fa-users" aria-hidden="true"></i></a>
    </li>
</ul>
