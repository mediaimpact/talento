<header class="container-fluid header fondoNegro">
	<div class="container">
		<div class="row">
			<div class="col-xs-2">
				<a href="{{ route('index')}}"><img src="{{asset('image/logo-mi-blanco.png')}}"></a>
			</div>
			<div class="col-xs-10 text-right">
				<ul>
					<li><a href="javascript:void(0)"><i class="icon-menu"></i></a></li>
                @guest
					<li><a href="{{route('login')}}">Iniciar Sesión</a></li>
					<li><a href="{{route('register')}}">Registrarse</a></li>		
				@else
                    <li>
                        @if( \Auth::user()->tipo == 1)
                           <a href="{{route('puestos')}}" >
                                {{ Auth::user()->name }} 
                            </a>   
                        @else
                            <a href="{{route('perfil')}}" >
                                {{ Auth::user()->name }} 
                            </a> 
                        @endif
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Cerrar Sesión
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endguest
                </ul>
			</div>
		</div>
	</div>
</header>
