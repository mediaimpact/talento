<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- CSRF Token -->
    <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
    <meta name="csrf-token" content="">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <!-- <title>{{ config('app.name', 'Postula') }}</title> -->
    <title>Postula</title>
    <!-- Styles -->
    <link href="{{ asset('image/favicon.png')}}" rel="shortcut icon">
    <link rel="apple-touch-icon" href="{{ asset('image/favicon.png')}}"/>
    <link href="{{asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/front/menu.css') }}" rel="stylesheet">
    <link href="{{asset('css/fonts.css') }}" rel="stylesheet">

  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi-talento/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi-talento/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi-talento/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi-talento/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi-talento/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi-talento/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi-talento/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi-talento/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi-talento/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi-talento/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi-talento/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi-talento/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi-talento/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('favi-talento/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

    @yield('css')
</head>
<body>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-84944885-3"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-84944885-3');
  </script>
    <div id="app">
        @include('layouts.header')
        @yield('content')
        <div id="menufixdt">
          <div class="menuBurger">
            <div class="row">
              <ul>
                <li><a href="{{route('index')}}#anclaNosotros" class="textoBurger MenuNosotros">Empresa</a></li>
                <li><a href="{{route('index')}}#anclaBeneficio" class="textoBurger MenuBeneficios">Beneficios</a></li>
                <li><a href="{{route('index')}}#anclaTestimonio" class="textoBurger MenuTestimonios">Testimonios</a></li>
                <li><a href="{{route('index')}}#anclaVacantes" class="textoBurger MenuVacantes">Vacantes</a></li>
                <li><a href="https://www.mediaimpact.pe/" target="_blank" class="textoBurger">Ir a la web</a></li>
                <li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
              </ul>
            </div>
            <a href="javascript:void(0)" class="cerrarBurger">X</a>
          </div>
        </div>
    </div>
    @include('layouts.footer')

    <!-- Scripts -->
    
  <!--   <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script> -->
   
    <script src="{{ asset('js/app.js') }}"></script>
     <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
      <script>
        $(document).ready(function(){
            $(".icon-menu").click(function(event) {
                $(".menuBurger").css({"display":"flex"});
                $(".menuBurger").css({"transition":"1s"});
                $(".menuBurger").css({"transform":"translate(0)"});   
                $(".menuBurger").addClass("tlmenu");
                $("body").css({"overflow": "hidden"});
                $("#menufixdt").css({"width":"100%"});
                $("#menufixdt").css({"opacity":"1"});
            });
            $(".cerrarBurger").click(function(event) {
                $(".menuBurger").css({"transition":"2s"}); 
                $(".menuBurger").css({"transform":"translate(100%)"});
                $(".menuBurger").removeClass("tlmenu");
                $("body").css({"overflow": "initial"});
                $("#menufixdt").css({"width":"0px"});
                $("#menufixdt").css({"transition": "all 1s linear"});
            });

            // $("#cerrarPopup").click(function(event){
            //     $("#popup").fadeOut("slow");
            // });

            $("#menufixdt").on("click",function(e) {
               var container = $(".menuBurger");
                if (!container.is(e.target) && container.has(e.target).length === 0) { 
                  $(".menuBurger").css({"transition":"1s"}); 
                  $(".menuBurger").css({"transform":"translate(100%)"});
                  $(".menuBurger").removeClass("tlmenu");
                  $("body").css({"overflow": "initial"});
                  $("#menufixdt").css({"width":"0px"});
                  $("#menufixdt").css({"transition": "all 1s linear"});             
                }
           });
        });
        
    </script>
    @yield('js')
    <!-- <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script> -->
</body>
</html>
