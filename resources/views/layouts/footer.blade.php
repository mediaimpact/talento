<footer class="container-fluid footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<p>Síguenos en</p>
				<a href="https://www.facebook.com/MediaImpactPeru/" target="_blank" class="icon-facebook"></a>
				<a href="https://www.linkedin.com/company/3356875/" target="_blank" class="icon-linkedin2"></a>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 copyRight">
				<p><a href="https://www.mediaimpact.pe" target="_blank" class="footerMi">Media Impact Perú SAC</a> - All rights reserved  2018</a></p>
			</div>
		</div>
	</div>
</footer>