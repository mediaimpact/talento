@extends('layouts.app')
@section('css')
<link href="{{asset('css/front/desc-vacantes.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-descVacantes">
		<div class="container-fluid fcoreVacantes">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2 text-center tituloVacantes">
					<h1>Vacantes</h1>
					<p>Requerimos talento humano que busque su desarrollo profesional y el incremento de sus capacidades.</p>
				</div>
			</div>
		</div>
		<div class="container-fluid fondoFiltro">
			<div class="container">
				<div class="row">
					<form action="{{route('buscarvacantes')}}" method="POST" id="frm-buscar">
						{{ csrf_field() }}
						<div class="col-lg-3 col-md-3 col-sm-12 textfiltro">
							<p>Filtro de <span>búsqueda</span></p>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-4">
							<label>Tipo</label>
							<select name="tipo">
								<option value=""></option>
								<option value="">Todo</option>
								@foreach($tipovacante as $tv)
								<option value="{{ $tv->id }}">{{ $tv->descripcion }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-4">
							<label>Tipo de empleo</label>
							<select name="categoria">
								<option value=""></option>
								@foreach($categorias as $ctg)
								<option value="{{ $ctg->id }}">{{ $ctg->descripcion }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-4">
							<label>Ubicación</label>
							<select name="distrito">
								<option value=""></option>
								@foreach($distritos as $dist)
								<option value="{{ $dist->id }}">{{ $dist->descripcion }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12">
							<button type="submit" class="btn btn-block btn-primary">Buscar</button>
						</div>
					</form>
				</div>				
			</div>
		</div>
		<div class="container-fluid inner-vacante">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm 12 col-xs-12">
						<a href="{{ route('vacantes') }}" class="atras"><  Regresar a listado de Vacantes</a>
					</div>
				</div>
				<div class="row">
				@if ($errors->has('email'))
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs12 alert alert-danger text-center">
						<p>{{ $errors->first('email') }}</p>
					</div>
                @endif
                @if ($errors->has('password'))
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs12 alert alert-danger text-center">
						<p>{{ $errors->first('password') }}</p>
					</div>
                @endif
				@if(isset($postulo) and $postulo != "")
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs12 alert alert-success text-center">
						<p>Acabas de Postular, felicidades nos pondremos en contacto con Ud.</p>
					</div>
				@endif
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12 container-descripcion">
						<div class="container-fluid inner-descripcion">
							<div class="container">
								<h2>{{ $vacante->puesto }}</h2>
								<img src="{{asset('image/ubicacion.png')}}"><span>{{ $vacante->ubicacion->descripcion }}</span>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fechaPublicacion">
								<div>
									<?php
										$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
										$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
										$fecha = strtotime($vacante->fecha_publicacion);
										$d = date('d', $fecha);
										$w = date('w', $fecha);
										$y = date('Y', $fecha);
										$n = date('n', $fecha);
									?>
									<img src="{{asset('image/calendario.png')}}">
									<p>Fecha de publicación: <span>{{ $dias[$w]." ".$d." de ".$meses[$n-1]. " del ".$y }}</span></p>
								</div>
								<div>
									<?php
										$fecha = strtotime($vacante->fecha_caducidad);
										$d = date('d', $fecha);
										$w = date('w', $fecha);
										$y = date('Y', $fecha);
										$n = date('n', $fecha);
									?>
									<img src="{{asset('image/calendario.png')}}">
									<p>Fecha de caducidad: <span>{{ $dias[$w]." ".$d." de ".$meses[$n-1]. " del ".$y }}</span></p>
								</div>
							
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid">
								<div class="separador"></div>
							</div>
						</div>
						@if($vacante->descripcion)
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 descripcion-vacante">
								<h4>Descripción</h4>
								{!! $vacante->descripcion !!}
							</div>
						</div>
						@endif
						@if($vacante->funciones)
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 funcion-vacante">
								<h4>Funciones</h4>
								{!! $vacante->funciones !!}
							</div>
						</div>
						@endif
						@if($vacante->requisitos)
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 requisitos-vacante">
								<h4>Requisitos:</h4>
								{!! $vacante->requisitos !!}
							</div>
						</div>
						@endif
						@if($vacante->beneficios)
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 beneficios-vacante">
								<h4>Beneficios:</h4>
								{!! $vacante->beneficios !!}
							</div>
						</div>
						@endif
						@if($vacante->horario)
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 horario-vacante">
								<h4>Horario:</h4>
								{!! $vacante->horario !!}
							</div>
						</div>
						@endif
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 postular-vacante">
								<!-- <a href="{{route('perfil')}}" id="postular">Postular</a> -->
								@if(\Auth::guest())
									<a href="#" id="postular">Postular</a>
								@else
									@if(isset($postulo) and $postulo != "")
										<a href="{{ route('historial')}}" id="preguntas">Ver postulaciones</a>
									@else
										@if($completar == 1)
										<a href="javascript:void(0)" id="completar">Postular</a>
										@else
										<a href="/cuentanos/{{ $vacante->slug }}" id="preguntas">Postular</a>
										@endif
									@endif
								@endif
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid">
								<div class="separador"></div>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 compartir-vacante">
								<p>Compartir en </p>
								<a href="http://www.facebook.com/sharer.php?u={{ url('/vacante/'.$vacante->slug)}}" class="share-link link-fb"><i class="icon-facebook"></i></a>
								<a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-12 containerTrelacionados">
						<div class="tituloPrelacionados">
							<h3>Puestos relacionados</h3>
						</div>
						@foreach($catvacante as $catvac)
						<div class="pRelacionados">
							<span>Publicado: {{ date("d/m/Y", strtotime($catvac->fecha_publicacion))}}</span>
							<h3>{{ $catvac->puesto }}</h3>
							<h4>{{ $catvac->ubicacion->descripcion }}</h4>
							<a href="/vacante/{{ $catvac->slug }}">Ver detalles</a>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup pop1">
	 	 		<div class="center-popup" >
	        		<div class="popup">
	        			<img src="{{asset('image/imglogin.png')}}">
	        			<div class="row box-frm1">
	        				<div class="col-md-12">
	        					<form class="form-horizontal" method="POST" action="{{ route('login') }}">
	        						{{ csrf_field() }}
	        						<input type="hidden" value="{{ $vacante->slug }}" name="slug">
	        						<input type="text" placeholder="Usuario" name="email" class="usuario">
	        						<input type="password" name="password" placeholder="Contraseña" class="contrasena">
	        						<button>Iniciar Sesión</button>
	        					</form>
	        				</div>
	        				<div class="col-md-12">
		        				<div class="col-xs-6">
		        					<input type="checkbox" name="">
		        					<label>Recordar mi contraseña</label>
		        				</div>
		        				<div class="col-xs-6 text-right">
		        					<a href="javascript:void(0)">¿Olvidaste tu contraseña?</a>
		        				</div>
	        				</div>
	        			</div>
	        			<div class="separador">
	        				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        					<h4>o</h4>
	        				</div>
	        			</div>
	        			<div class="facebook row">
	        				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        					<a href="{{asset('login/facebook?slug='.$vacante->slug)}}"><i class="icon-facebook"></i> <span>Continue with facebook</span></a>
	        				</div>
	        			</div>
	        			<div class="registrate">
	        				<div class="col-md-12 text-center">
	        					<span>¿Aún no tienes usuario?</span>
	        					<a href="{{route('register')}}">Regístrate</a>
	        				</div>
	        			</div>
	          			<a href="javascript:void(0)" id="cerrarPopup" class="close">X</a>
	        		</div>
	      		</div>
	    </div>

		<div id="popup" class="inner-popup pop2">
	 	 		<div class="center-popup" >
	        		<div class="popup">
	        			<img src="{{asset('image/imglogin.png')}}">
	        			<div class="row box-frm1">
	        				<div class="col-md-12">
	        					<form class="form-horizontal" method="POST" action="{{ route('login') }}">
	        						{{ csrf_field() }}
	        						<p class="text-center">Antes de postular es necesario subir tu CV.</p>
	        						<a href="{{route('perfil')}}" style="color: #fff;
    font: 20px ubunturegular;
    background: #d00;
    margin: 40px auto 30px auto;
    border-radius: 8px;
    padding: 8px 0;
    text-align: center;text-decoration: none;">Completar mis datos</a>
	        					</form>
	        				</div>
	        			</div>
	          			<a href="javascript:void(0)" id="cerrarPopup" class="close">X</a>
	        		</div>
	      		</div>
	    </div>

	</section>
@endsection
@section("js")
	<script type="text/javascript">
	$(document).ready(function(){
		if (window.location.hash == '#_=_'){
		    if (history.replaceState) {
		        var cleanHref = window.location.href.split('#')[0];
		        history.replaceState(null, null, cleanHref);

		    } else {
		        window.location.hash = '';
		    }
		}
		$(".close").click(function(event){
            $(".pop1").fadeOut("slow");
            $(".pop2").fadeOut("slow");
            $("body").css({"overflow": "initial"});
        });
		$("#postular").click(function(event){
          event.preventDefault();
          $(".pop1").fadeIn("slow");
          $("body").css({"overflow": "hidden"});
        });
		$("#completar").click(function(event){
          event.preventDefault();
          $(".pop2").fadeIn("slow");
          $("body").css({"overflow": "hidden"});
        });

	});

	</script>
@endsection