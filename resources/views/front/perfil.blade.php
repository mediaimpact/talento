@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="{{asset('css/front/perfil.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-perfil">
		<div class="container-fluid fcorePerfil">
		</div>
		<div class="container-fluid inner-perfil">
			<div class="container">
				<div class="row h-top-perfil">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 header-perfil">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-xs-8">
								<h2>{{  \Auth::user()->name }} {{ \Auth::user()->apellido }} <!-- <i class="fa fa-pencil"></i> --></h2>
								<p><i class="fa fa-envelope-o"></i> {{  \Auth::user()->email }}</p>
								<p><i class="fa fa-mobile-phone"></i> {{  \Auth::user()->telefono }}</p>
							</div>
							<div class="col-lg-4 col-md-4 col-xs-4 text-center bg-blanco">
								<figure>
									@if(substr(\Auth::user()->photo, 0, 3) == 'htt')
										<img src="{{\Auth::user()->photo}}" alt="avatar" class="img-circle">
									@else
										@if(\Auth::user()->photo)
										<img src="{{asset('image/photo/'.\Auth::user()->photo)}}" alt="avatar" class="img-circle">
										@else
										<img src="" alt="" class="img-circle">
										@endif
									@endif
									<p>Actualizar foto<br>*Archivos menores a 2mb</p>
								</figure>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 fichaPerfil">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<ul class="nav nav-tabs" role="tablist">
									    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos</a></li>
									    <li role="presentation"><a href="{{ route('historial')}}">Historial de postulaciones</a></li>
									</ul>
								</div>
								<div class="row">
								 <div class="tab-content">
								    <div role="tabpanel" class="tab-pane active" id="home">
								    	<div class="col-lg-6 col-lg-offset-3">
											<form action="{{route('update_perfil')}}" id="perfil" method="post" enctype="multipart/form-data">
												{{ csrf_field() }}
												@if (session('status'))
												    <div class="alert alert-success alert-dismissible fade in" role="alert"> 
														<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
														<strong>Exito</strong> Perfil Actualizado.
													</div>
												@endif
												<label for="">Nombres</label>
												<input type="text" name="name" value="{{ $dato->name }}">
												<label for="">Apellidos</label>
												<input type="" name="apellido" value="{{ $dato->apellido }}">
												<label for="">Teléfono</label>
												<input type="" name="telefono" value="{{ $dato->telefono }}">
												<label for="">Email</label>
												<input type="email" name="email" value="{{ $dato->email }}">
												<div class="inner-rbt">
													<div class="columna"><label for="">Sexo</label></div>
													<div class="columna">
														@if($dato->sexo)
															<label class="checkbox-inline">
															  <input class="varon" type="radio" name="sexo" value="F" @if($dato->sexo == "F") checked @endif > Femenino
															</label>
															<label class="checkbox-inline">
															  <input class="dama" type="radio" name="sexo" value="M" @if($dato->sexo == "M") checked @endif > Masculino
															</label>
														@else
															<label class="checkbox-inline">
															  <input class="varon" type="radio" name="sexo" value="F" checked > Femenino
															</label>
															<label class="checkbox-inline">
															  <input class="dama" type="radio" name="sexo" value="M"> Masculino
															</label>
														@endif
													</div>
												</div>
												<label for="">Área de interés</label>
												<select name="area_id">
													<option value=""></option>
													@foreach($areas as $area)
														@if($dato->area_id)
															@if($dato->area_id == $area->id)
																<option value="{{ $area->id }}" selected>{{ $area->descripcion }}</option>
															@else
																<option value="{{ $area->id }}">{{ $area->descripcion }}</option>
															@endif
														@else
														<option value="{{ $area->id }}">{{ $area->descripcion }}</option>
														@endif
													@endforeach
												</select>
												<label for="">Expectativa salarial (S/.)</label>
												<input type="" name="sueldo" value="{{ $dato->sueldo }}">
												<label for="">Adjunta tu Linkedin</label>
												<input type="" name="linkedin" value="{{ $dato->linkedin }}">
												<label for="">Carga tu CV</label>
												<label for="file" class="file_label">
													@if($dato->cv )
														{{ $dato->cv }}
													@else
													Carga tu CV
							                        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
							                        <span>*Subir archivos menores a 2mb</span>
							                        @endif
							                    </label>
							                    @if($dato->cv)
							                    	<input id="file" type="file" name="cvn" value="{{ $dato->cv }}" aria-required="true" class="valid" aria-invalid="false">
							                    @else
							                    	<input id="file" type="file" name="cv" />
							                    @endif
							                    <div class="row"><div class="col-md-12"></div></div>
							                    <div class="row">
							                    	<div class="col-md-12 boxfoto">
							                    		<div class="row">
								                    	<span for="" class="col-xs-5">Foto</span>
								                    	<div class="col-xs-6 avatar">
								                    		@if(substr(\Auth::user()->photo, 0, 3) == 'htt')
																<img src="{{\Auth::user()->photo}}" id="des_url1" alt="avatar" class="img-circle">
															@else
																@if(\Auth::user()->photo)
																<img src="{{asset('image/photo/'.\Auth::user()->photo)}}" id="des_url1" alt="avatar" class="img-circle">
																@else
																<img src="{{asset('image/select.png')}}" id="des_url1" alt="">
																@endif
															@endif
								                    		
									                    	<input id="file_url1" class="col-md-6" type="file" name="photo" />
								                    	</div>
								                    	</div>
							                    	</div>
							                    </div>
							                    <div>
													<label for="">Nueva Contraseña</label>
							                    	<input type="password" name="newpassword">
							                    </div>
							                    <input class="boton" type="submit" id="enviarDatos" value="Enviar datos" ></input>
											</form>
										</div>
								    </div>
								  </div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup">
	 	 		<div class="center-popup" >
	        		<div class="popup">
	        			<img src="{{asset('image/trofeo.png')}}">
	        			<div class="row">
	        				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
	        					<h2>¡Hemos registrado tus datos con éxito!</h2>
	        					<p>Revisa tu correo para confirmar la carga de datos, nos comunicaremos contigo pronto</p>
	        					<a href="{{route('vacantes')}}" id="cerrarPopup">OK</a>
	        				</div>
	        			</div>
	        		</div>
	      		</div>
	    </div>	
	</section>
@endsection
@section("js")
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/iFile.js')}}"></script>
	<script src="{{asset('js/perfil.js')}}"></script>
@endsection