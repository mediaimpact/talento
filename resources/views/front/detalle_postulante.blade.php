@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<link href="{{asset('css/front/puestos.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-perfil">
		<div class="container-fluid fcorePerfil text-center">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 text-center tituloVacantes">
					<h1>{{ $dato->name }}</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid inner-perfil">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fichaPerfil">
						<div class="row">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
								@include('layouts.menuadm')
							</div>
							<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
								<div class="row">
								  <div class="tab-content">
								    <div role="tabpanel" class="tab-pane active" id="postulantes">
								    	<div class="row">
							    			<div class="col-lg-12">
							    				<div class="inner-descripcion">
													<h2>{{ $dato->name }} {{ $dato->apellido }}</h2>
													<hr>
												</div>
							    			</div>
							    			<div class="paso1">
								    			<div class="col-lg-12">
								    				<div class="row">
														<div class="col-lg-6 container-descripcion">
															<label for="">Teléfono</label>
															<p>{{ $dato->telefono }}</p>
															<label for="">Email</label>
															<p>{{ $dato->email }}</p>
															<label for="">Sexo</label>
															<p>
																@if($dato->sexo == "F")  
																	Femenino
																@else
																	Masculino
																@endif
															</p>
															<label for="">Área</label>
															<p>
																@foreach($areas as $area)
																	@if($dato->area_id)
																		@if($dato->area_id == $area->id)
																			{{ $area->descripcion }}
																		@endif
																	@endif
																@endforeach
															</p>
															<label for="">Expectativa salarial (S/.)</label>
															<p>{{ $dato->sueldo }}</p>
															<label for="">Adjunta tu Linkedin</label>
															<p><a href="{{ $dato->linkedin }}" target="_blank">{{ $dato->linkedin }}</a></p>
															<label for="">CV:</label>
															<p><a href="{{asset('/cv/'.$dato->cv)}}" target="_blank">Descargar <i class="fa fa-file"></i></a></p>
														</div>
														<div class="col-lg-6 container-descripcio">
															<h4>Puesto que Postulo</h4>
															<label for="">Puesto</label>
															<p>{{ $postulacion->vacante->puesto }}</p>
															<label for="">Descripción</label>
															<p>{!! $postulacion->vacante->descripcion !!}</p>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"></div>
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
															<a href="javascript:history.back(1)" class="btn">Volver Atrás</a>
														</div>
													</div>
								    				
								    			</div>

							    			</div>
								    	</div>
								    </div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup">
 	 		<div class="center-popup" >
        		<div class="popup">
        			<img src="{{asset('image/trofeo.png')}}">
        			<div class="row">
        				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
        					<h2>¡Hemos registrado tus datos con éxito!</h2>
        					<p>Revisa tu correo para confirmar la carga de datos, nos comunicaremos contigo pronto</p>
        					<a href="{{route('vacantes')}}" id="cerrarPopup">OK</a>
        				</div>
        			</div>
        		</div>
      		</div>
	    </div>	
		<div class="menuBurger">
			<div class="row">
				<ul>
					<li><a href="javascript:void(0)" class="textoBurger MenuNosotros">Empresa</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuBeneficios">Beneficios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuTestimonios">Testimonios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuVacantes">Vacantes</a></li>
					<li><a href="javascript:void(0)" class="textoBurger">Ir a la web</a></li>
					<li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>
			<a href="javascript:void(0)" class="cerrarBurger">X</a>
		</div>
	</section>
@endsection
@section("js")
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/iFile.js')}}"></script>
	<script src="{{asset('js/perfil.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>
	<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    
	<script>
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip();
		});

		$(".dpreguntas").click(function(){
			$(".paso1").css('display','none');
			$(".paso2").css('display','block');
		});
		$(".atras").click(function(){
			$(".paso1").css('display','block');
			$(".paso2").css('display','none');
		});
	</script>
@endsection