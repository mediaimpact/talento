@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<link href="{{asset('css/front/puestos.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-perfil">
		<div class="container-fluid fcorePerfil text-center">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 text-center tituloVacantes">
					<h1>{{ $vacante->puesto }}</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid inner-perfil">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fichaPerfil">
						<div class="row">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
								@include('layouts.menuadm')
							</div>
							<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
								<div class="row">
								  <div class="tab-content">
								    <div role="tabpanel" class="tab-pane active" id="postulantes">
								    	@if (session('status'))
											@if(session('status') == "exito")
												<div class="alert alert-success alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Exito</strong> Nuevo puesto creado.
												</div>
											@else
												<div class="alert alert-danger alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Error</strong> Puesto no creado.
												</div>
											@endif
										@endif
								    	
								    	<div class="row">
							    			<div class="col-lg-12">
							    				<div class="inner-descripcion">
													<h2>{{ $vacante->puesto }}</h2>
													<img src="{{asset('image/ubicacion.png')}}"><span>{{ $vacante->ubicacion->descripcion }}</span>
													<hr>
												</div>
							    			</div>
							    			<div class="paso1">
								    			<div class="col-lg-12">
								    				<div class="row">
														<div class="col-lg-12 container-descripcion">
															<div class="row detalle-descripcion">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fechaPublicacion">
																	<div>
																		<?php
																			$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
																			$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
																			$fecha = strtotime($vacante->fecha_publicacion);
																			$d = date('d', $fecha);
																			$w = date('w', $fecha);
																			$y = date('Y', $fecha);
																			$n = date('n', $fecha);
																		?>
																		
																		<p> <img src="{{asset('image/calendario.png')}}"> Fecha de publicación: <span>{{ $dias[$w]." ".$d." de ".$meses[$n-1]. " del ".$y }}</span></p>
																	</div>
																	<div>
																		<?php
																			$fecha = strtotime($vacante->fecha_caducidad);
																			$d = date('d', $fecha);
																			$w = date('w', $fecha);
																			$y = date('Y', $fecha);
																			$n = date('n', $fecha);
																		?>
																		
																		<p><img src="{{asset('image/calendario.png')}}"> Fecha de caducidad: <span>{{ $dias[$w]." ".$d." de ".$meses[$n-1]. " del ".$y }}</span></p>
																	</div>
																
																</div>
															</div>
															<div class="row seccion-descripcion">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid">
																	<div class="separador"></div>
																</div>
															</div>
															@if($vacante->descripcion)
															<div class="row seccion-descripcion">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 descripcion-vacante">
																	<h4>Descripción</h4>
																	{!! $vacante->descripcion !!}
																</div>
															</div>
															@endif
															@if($vacante->funciones)
															<div class="row seccion-descripcion">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 funcion-vacante">
																	<h4>Funciones</h4>
																	{!! $vacante->funciones !!}
																</div>
															</div>
															@endif
															@if($vacante->requisitos)
															<div class="row seccion-descripcion">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 requisitos-vacante">
																	<h4>Requisitos:</h4>
																	{!! $vacante->requisitos !!}
																</div>
															</div>
															@endif
															@if($vacante->beneficios)
															<div class="row seccion-descripcion">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 beneficios-vacante">
																	<h4>Beneficios:</h4>
																	{!! $vacante->beneficios !!}
																</div>
															</div>
															@endif
															@if($vacante->horario)
															<div class="row seccion-descripcion">
																<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 horario-vacante">
																	<h4>Horario:</h4>
																	{!! $vacante->horario !!}
																</div>
															</div>
															@endif
															<div class="row seccion-descripcion">
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"></div>
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
																	<a href="javascript:void(0)" class="btn btn-rojo dpreguntas">Preguntas</a>
																</div>
															</div>
														</div>
													</div>
								    			</div>
							    			</div>
											<div class="paso2">
												<div class="col-lg-12">
													<div class="row">
														<?php $i = 1;?>
														@foreach($preguntas as $pregunta)
														    <div class="col-lg-6">
																<div class="form-group">
																    <h4>{{ $i }}. {{ $pregunta->descripcion }} </h4>
																    <ul>
																    	@foreach($pregunta->respuestas as $opcion)
																    	<li>{{ $opcion->descripcion }}</li>
																    	@endforeach
																    </ul>
																</div>
															</div>
															@if( ($i % 2) == 0)
																<div class="col-lg-12"></div>
															@endif
															<?php $i++;?>
														@endforeach
													</div>
													<div id="maspreguntas"></div>
													<div class="row">
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"></div>
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
															<a href="javascript:void(0)" class="btn btn-rojo atras">Atrás</a>
														</div>
													</div>
												</div>
											</div>
								    	</div>
								    </div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup">
 	 		<div class="center-popup" >
        		<div class="popup">
        			<img src="{{asset('image/trofeo.png')}}">
        			<div class="row">
        				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
        					<h2>¡Hemos registrado tus datos con éxito!</h2>
        					<p>Revisa tu correo para confirmar la carga de datos, nos comunicaremos contigo pronto</p>
        					<a href="{{route('vacantes')}}" id="cerrarPopup">OK</a>
        				</div>
        			</div>
        		</div>
      		</div>
	    </div>	
		<div class="menuBurger">
			<div class="row">
				<ul>
					<li><a href="javascript:void(0)" class="textoBurger MenuNosotros">Empresa</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuBeneficios">Beneficios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuTestimonios">Testimonios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuVacantes">Vacantes</a></li>
					<li><a href="javascript:void(0)" class="textoBurger">Ir a la web</a></li>
					<li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>
			<a href="javascript:void(0)" class="cerrarBurger">X</a>
		</div>
	</section>
@endsection
@section("js")
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/iFile.js')}}"></script>
	<script src="{{asset('js/perfil.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>
	<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    
	<script>
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip();
		});

		$(".dpreguntas").click(function(){
			$(".paso1").css('display','none');
			$(".paso2").css('display','block');
		});
		$(".atras").click(function(){
			$(".paso1").css('display','block');
			$(".paso2").css('display','none');
		});
	</script>
@endsection