@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="{{asset('css/front/postulantes_vacantes.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-vacantes">
		<div class="container-fluid fcoreVacantes">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 text-center tituloVacantes">
					<h1>Vacantes</h1>
				</div>
			</div>
		</div>
		{{--<div class="container-fluid fondoFiltro">
			<div class="container">
				<div class="row">
					<form action="{{route('buscarvacantes')}}" method="POST" id="frm-buscar">
						{{ csrf_field() }}
						<div class="col-lg-3 col-md-3 col-sm-12 textfiltro">
							<p>Filtro de <span>búsqueda</span></p>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-4">
							<label>Tipo</label>
							<select name="tipo">
								<option value=""></option>
								<option value="">Todo</option>
								@foreach($tipovacante as $tv)
								<option value="{{ $tv->id }}">{{ $tv->descripcion }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-4">
							<label>Tipo de empleo</label>
							<select name="categoria">
								<option value=""></option>
								@foreach($categorias as $ctg)
								<option value="{{ $ctg->id }}">{{ $ctg->descripcion }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-4">
							<label>Ubicación</label>
							<select name="distrito">
								<option value=""></option>
								@foreach($distritos as $dist)
								<option value="{{ $dist->id }}">{{ $dist->descripcion }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12">
							<button type="submit" class="btn btn-block btn-primary">Buscar</button>
						</div>
					</form>
				</div>				
			</div>
		</div>--}}
		<div class="container-fluid inner-vacante">
			<div class="container fichaPerfil">
				<div class="row">
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						@include('layouts.menuadm')
					</div>
					<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
						<div class="row">
							<div class="col-lg-12">
								<h4>Seleccione vacante, para poder ver los postulantes que postularón al puesto</h4>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<p>Mostrar <span>{{ $vacantes->lastItem() }}</span> de {{ $vacantes->total() }}</p>
							</div>
							{{--<div class="col-lg-2 col-lg-offset-4 col-md-2 col-md-offset-4 col-sm-4 text-right">
								<p class="tBusqueda">Búsqueda rápida</p>		
							</div>
							<div class="col-lg-3 col-md-3 col-sm-5">
								<form action="{{route('searchvacantes')}}" method="POST" id="frm-search">
									{{ csrf_field() }}
									<input type="text" name="search">
								</form>
							</div>--}}
						</div>
						<div class="row">
							@foreach($vacantes as $vacante)
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="descVacante">
									<span>Fecha de aviso: {{ date("d/m/Y", strtotime($vacante->fecha_publicacion))}}</span>
									<h3>{{ $vacante->puesto }}</h3>
									<h5>{{ $vacante->ubicacion->descripcion}}</h5>
									<?php
										$text = strip_tags($vacante->descripcion);
										$text = substr($text, 0, 120);
									?>
									<p>{{ $text }} ...</p>
									
									<a href="{{route('filtrolist', $vacante->id)}}">Ver postulantes</a>
								</div>
							</div>
							@endforeach
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
								{{$vacantes}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section("js")
@endsection