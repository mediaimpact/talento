@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="{{asset('css/front/perfil.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-perfil">
		<div class="container-fluid fcorePerfil">
		</div>
		<div class="container-fluid inner-perfil">
			<div class="container">
				<div class="row h-top-perfil">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 header-perfil">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-xs-8">
								<h2>{{  \Auth::user()->name }} {{ \Auth::user()->apellido }} <!-- <i class="fa fa-pencil"></i> --></h2>
								<p><i class="fa fa-envelope-o"></i> {{  \Auth::user()->email }}</p>
								<p><i class="fa fa-mobile-phone"></i> {{  \Auth::user()->telefono }}</p>
							</div>
							<div class="col-lg-4 col-md-4 col-xs-4 text-center bg-blanco">
								<figure>
									@if(substr(\Auth::user()->photo, 0, 3) == 'htt')
										<img src="{{\Auth::user()->photo}}" alt="avatar" class="img-circle">
									@else
										@if(\Auth::user()->photo)
										<img src="{{asset('image/photo/'.\Auth::user()->photo)}}" alt="avatar" class="img-circle">
										@else
										<img src="" alt="" class="img-circle">
										@endif
									@endif
									<p>Actualizar foto<br>*Archivos menores a 2mb</p>
								</figure>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 fichaPerfil">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<ul class="nav nav-tabs" role="tablist">
									    <li role="presentation" ><a href="{{route('perfil')}}" >Datos</a></li>
									    <li role="presentation" class="active"><a href="#historial" aria-controls="historial" role="tab" data-toggle="tab">Historial de postulaciones</a></li>
									</ul>
								</div>
								<div class="row">
								 <div class="tab-content">
								    <div role="tabpanel" class="tab-pane active" id="historial">
								    	@if (session('status'))
											@if(session('status') == "exito")
												<div class="alert alert-success alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Exito</strong> La postulacion ha sido eliminada.
												</div>
											@else
												<div class="alert alert-danger alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Error</strong> Postulacion no eliminada.
												</div>
											@endif
										@endif
								    	@foreach($historial as $htl)
								    	<div class="row">
								    		<div class="col-lg-12">
												<div class="row">
													<div class="col-md-8">
														<h2>{{ $htl->puesto }}</h2>
														<h3>{{ $htl->ubicacion }}</h3>
														<p>Fecha de postulación: {{ $htl->fecha}}7</p>
													</div>
													<div class="col-md-2">
														<a href="/vacante/{{ $htl->slug }}" class="btn btn-block">Ver detalle</a>
													</div>
													<div class="col-md-2 text-center">
														<a href="{{ route('eliminar',$htl->id)}}"><i class="fa fa-trash-o"></i></a>
													</div>
												</div>
											</div>
								    	</div>
								    	@endforeach
								    	<div class="col-md-12 text-center">
								    		{{ $historial }}
								    	</div>
								    </div>
								  </div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup">
	 	 		<div class="center-popup" >
	        		<div class="popup">
	        			<img src="{{asset('image/trofeo.png')}}">
	        			<div class="row">
	        				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
	        					<h2>¡Hemos registrado tus datos con éxito!</h2>
	        					<p>Revisa tu correo para confirmar la carga de datos, nos comunicaremos contigo pronto</p>
	        					<a href="{{route('vacantes')}}" id="cerrarPopup">OK</a>
	        				</div>
	        			</div>
	        		</div>
	      		</div>
	    </div>	
		<div class="menuBurger">
			<div class="row">
				<ul>
					<li><a href="javascript:void(0)" class="textoBurger MenuNosotros">Empresa</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuBeneficios">Beneficios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuTestimonios">Testimonios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuVacantes">Vacantes</a></li>
					<li><a href="javascript:void(0)" class="textoBurger">Ir a la web</a></li>
					<li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>
			<a href="javascript:void(0)" class="cerrarBurger">X</a>
		</div>
	</section>
@endsection
@section("js")
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/iFile.js')}}"></script>
	<script src="{{asset('js/perfil.js')}}"></script>
@endsection