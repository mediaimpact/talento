@extends('layouts.app')
@section('css')
<link href="{{asset('css/front/inicio.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="inicio" id="anclaInicio">
		<div class="container inner-container">
			<div>
				<h1>Postula.pe</h1>
			</div>
		</div>
		<div class="container menu">
			<div class="row">
				<div class="col-lg-12">
					<nav>
						<ul>
							<li>
								<a href="javascript:void(0)" class="MenuNosotros">Nosotros</a>
								<div></div>
							</li>
							<li>
								<a href="javascript:void(0)" class="MenuBeneficios">Beneficios</a>
								<div></div>
							</li>
							<li>
								<a href="javascript:void(0)" class="MenuTestimonios">Testimonios</a>
								<div></div>
							</li>
							<li>
								<a href="javascript:void(0)" class="MenuVacantes">Vacantes</a>
								<div></div>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2 col-lg-offset-5 col-md-2 col-md-offset-5 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5">
					<a href="javascript:void(0)" class="arrow MenuNosotros"><img src="{{asset('image/downwhite.png')}}" alt=""></a>
				</div>
			</div>
		</div>
	</section>
	<section class="nosotros" id="anclaNosotros">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2>Empresa</h2>
					<div class="descripcion">
						<h4>Somos un equipo</h4>
						<p>En Media Impact, amamos lo que hacemos y eso vuelve más divertido el trabajo. Compartimos momentos de entretenimiento sin perder el foco de cada proyecto.</p>
						<a href="https://www.mediaimpact.pe/nosotros" target="_black">Conoce más de nosotros></a>
					</div>
				</div>
				<div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 text-center">
					<img src="{{asset('image/fotonosotros.png')}}" alt="">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2 col-lg-offset-5 col-md-2 col-md-offset-5 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5">
					<a href="javascript:void(0)" class="arrow MenuBeneficios"><img src="{{asset('image/downgray.png')}}" alt=""></a>
				</div>
			</div>
		</div>
	</section>
	<section class="beneficios" id="anclaBeneficio">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 text-center">
					<h2>Beneficios</h2>
					<div class="descripcion">
						<p>Queremos que te sientas cómodo en la agencia y cuentes con nuestro apoyo para tu desarrollo personal y profesional.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid inner-beneficios">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 text-right benLeft">
					<ul>
						<li><span class="circle fondoCircle"></span><a href="javascript:void(0)" class="textoCircle active" data-id="1">Gratificaciones</a></li>
						<li><span class="circle"></span><a href="javascript:void(0)" class="textoCircle" data-id="2">Vacaciones</a></li>
						<li><span class="circle"></span><a href="javascript:void(0)" class="textoCircle" data-id="3">Buen clima laboral</a></li>
						<li><span class="circle"></span><a href="javascript:void(0)" class="textoCircle" data-id="4">Red de reconocimiento<br>interno</a></li>
					</ul>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<div class="master-circle">
						<div class="inner-circle">
							<img src="{{asset('image/servicio1.png')}}" id="mostrarImg">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 benRight">
					<ul>
						<li><a href="javascript:void(0)" class="textoCircle" data-id="5">Línea de carrera</a><span class="circle"></span></li>
						<li><a href="javascript:void(0)" class="textoCircle" data-id="6">Beneficios de ley</a><span class="circle"></span></li>
						<li><a href="javascript:void(0)" class="textoCircle" data-id="7">Actividades de integración</a><span class="circle"></span></li>
						<li><a href="javascript:void(0)" class="textoCircle" data-id="8">Capacitaciones online</a><span class="circle"></span>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2 col-lg-offset-5 col-md-2 col-md-offset-5 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5">
					<a href="javascript:void(0)" class="arrow MenuTestimonios"><img src="{{asset('image/downwhite.png')}}" alt=""></a>
				</div>
			</div>
		</div>
	</section>
	<section class="testimonio" id="anclaTestimonio">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2>Testimonios</h2>
				</div>
			</div>
			<div class="row">
				<ul class="adaptive">
					<li>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 sliderImg">
							<div class="inner-testimonio">
								<div class="fila">
									<h4>Pasión <span>y </span>dedicación</h4>
								</div>
								<div class="fila">
									<h5>Freddy Morales</h5>
								</div>
								<div class="fila">
									<h6>Creative Director</h6>
								</div>
								<!-- <div class="fila">
									<h6>DNI: 10293847</h6>
								</div> -->
								<div class="fila">
									<p>Cuando entré a Media Impact, me di cuenta que me esperaban grandes retos y no me equivoqué. Cada día tenemos que vencer nuevos desafíos; en ese proceso, aprovecho para conocer mejor a todo el equipo y aprender de su experiencia. ¡El talento nos sobra!</p>
								</div>
									
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 innerPersona">
							<img src="{{asset('image/Freddy.png')}}">
							<a href="javascript:void(0)" class="btnPlay" data-id="slider1"><img src="{{asset('image/play.png')}}"></a>
						</div>
					</li>
					<li>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 sliderImg">
							<div class="inner-testimonio">
								<div class="fila">
									<h4>Pasión <span>y </span>dedicación</h4>
								</div>
								<div class="fila">
									<h5>Ana Sánchez</h5>
								</div>
								<div class="fila">
									<h6>Graphic Designer</h6>
								</div>
								<!-- <div class="fila">
									<h6>DNI: 10293847</h6>
								</div> -->
								<div class="fila">
									<p>En Media Impact no sólo conocí a muchas personas que me ayudaron a tener un mejor panorama sobre mi carrera profesional, sino que además se volvieron mis amigos. Desde que ingresé a la agencia hemos dado grandes pasos ¡y vamos por muchos más!</p>
								</div>
									
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 innerPersona">
							<img src="{{asset('image/ana.png')}}">
							<a href="javascript:void(0)" class="btnPlay" data-id="slider2"><img src="{{asset('image/play.png')}}"></a>
						</div>
					</li>
			</div>
		</div>
		<a href="javascript:void(0)" class="arrow MenuVacantes"><img src="{{asset('image/downgray.png')}}" alt=""></a>
		<div id="popup" class="inner-popup">
 	 		<div class="center-popup" >
        		<div class="popup" >
        			<iframe width="560" height="315" src="" frameborder="0" gesture="media" allowfullscreen></iframe>
          			<a href="javascript:void(0)" id="cerrarPopup">X</a>
        		</div>
      		</div>
    	</div>
	</section>
	<section class="vacantes" id="anclaVacantes">
		<div class="container-fluid fondoVacante">
			<div class="row">
				<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 inner-text">
					<h2>Vacantes</h2>
					<p>Mira los puestos que tenemos para ti.</p>
				</div>
				
			</div>
		</div>
		<div class="container-fluid tiposVacantes">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 text-center">
					<ul>
						<li>
							<a href="{{route('parttime')}}">
								<img src="{{asset('image/ico-part-time.png')}}">
								<span>Part-Time</span>
							</a>
						</li>
						<li>
							<a href="{{route('fulltime')}}">
								<img src="{{asset('image/ico-full-time.png')}}">
								<span>Full-Time</span>
							</a>
						</li>
						<li>
							<a href="{{route('adminis')}}">
								<img src="{{asset('image/ico-administrativo.png')}}">
								<span>Administrativo</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
@endsection
@section("js")
	<script>
    $(document).ready(function(){
       $("#cerrarPopup").click(function(event){
            $("#popup").fadeOut("slow");
            $("iframe").attr('src','');
        });
         $(".btnPlay").click(function(event){
            $("#popup").css({"display": "block"});
            b = $(this).attr("data-id");
            if(b == "slider1"){
                $("iframe").attr('src','https://www.youtube.com/embed/kZWyQqPH5bo?ecver=1');
            }
            if(b == "slider2"){
                $("iframe").attr('src','https://www.youtube.com/embed/Be4N5brxXbg?ecver=1');
            }
            if(b == "slider3"){
                $("iframe").attr('src','https://www.youtube.com/embed/vv9tOx3dMEo?ecver=1');
            }
            if(b == "slider4"){
                $("iframe").attr('src','https://www.youtube.com/embed/_GPkC2JkFow?ecver=1');
            }
        });
        $('.adaptive').lightSlider({
          adaptiveHeight:true,
          auto:true,
          item:1,
          slideMargin:0,
          loop:true,
          pause: 4000,
          speed: 1000
        });



        var h;
        h = $(window).height();
        console.log(h+'px');
        $(".inicio").css({"height": h});
        
        $(window).scroll(function() {
            var move_y;
            move_y = window.pageYOffset;
            if (move_y < 600) {
                $(".header").css({"position":"absolute"});
                $(".header").removeClass("fondoRojo");
                $(".header").addClass("fondoNegro");
                $(".header a img").attr("src","image/logo-mi-blanco.png");
                $(".header").css({"height":"initial"});
            } 
            if (move_y > 600) {
                $(".header").removeClass("fondoNegro");
                //$(".header").css({"height":"8%"});
                $(".header").addClass("fondoRojo");
                $(".header").css({"position":"fixed"});
                $(".header a img").attr("src","image/logo-mi-blanco-horizontal.png");
            }
            if (move_y > 1250) {
                $(".header").removeClass("fondoRojo");
                $(".header").addClass("fondoNegro");
            } 
            if (move_y > 1850) {
                $(".header").removeClass("fondoNegro");
                $(".header").addClass("fondoRojo");
            }
            if (move_y > 2670) {
                $(".header").removeClass("fondoRojo");
                $(".header").addClass("fondoNegro");
            }
        });
        $(".icon-menu").click(function(event) {
            $(".menuBurger").css({"display":"flex"});
            $(".menuBurger").css({"transition":"1s"});
            $(".menuBurger").css({"transform":"translate(0)"});   
            $(".menuBurger").css({"width":"30%"});
            $("body").css({"overflow": "hidden"});
        });
        $(".cerrarBurger").click(function(event) {
            $(".menuBurger").css({"transition":"1s"}); 
            $(".menuBurger").css({"transform":"translate(100%)"});
            $("body").css({"overflow": "initial"});
        });
        $(".beneficios .inner-beneficios .benLeft ul li a").on('click',function(){
            $(".beneficios .inner-beneficios .benLeft ul li a").removeClass('active');
            $(".beneficios .inner-beneficios .benLeft ul li span").removeClass('fondoCircle');
            $(this).addClass('active');
            $(this).parent().children('span').addClass('fondoCircle');
            });
        $(".beneficios .inner-beneficios ul li a").on('click',function(){
            $(".beneficios .inner-beneficios ul li a").removeClass('active');
            $(".beneficios .inner-beneficios ul li span").removeClass('fondoCircle');
            $(this).addClass('active');
            $(this).parent().children('span').addClass('fondoCircle');
            a = $(this).attr("data-id");
            if(a == 1){
                $('#mostrarImg').attr('src','image/servicio1.png');
            }
            if(a == 2){
                $('#mostrarImg').attr('src','image/familia.jpg');
            }
            if(a == 3){
                $('#mostrarImg').attr('src','image/epps.jpg');

            }
            if(a == 4){
                $('#mostrarImg').attr('src','image/gratificacion.jpg');

            }
            if(a == 5){
                $('#mostrarImg').attr('src','image/uniformes.jpg');

            }
            if(a == 6){
                $('#mostrarImg').attr('src','image/vacaciones.jpg');

            }
            if(a == 7){
                $('#mostrarImg').attr('src','image/clima-laboral.jpg');

            }
            if(a == 8){
                $('#mostrarImg').attr('src','image/oferta.jpg');

            }
        });
        function cerrarDelay() {
            setTimeout(function(){ 
                $(".menuBurger").css({"transform":"translate(100%)"});
                $("body").css({"overflow":"initial"});
                $("#menufixdt").css({"display":"none"});
                 }, 2500);
        }
        $(".MenuNosotros").click(function() {
            if ($("#anclaNosotros").length > 0){
              topScroll = "";
              timeOut = ""; 
              htmlbody = $('html,body'); 
              valor = 0; 
              topScroll = $("#anclaNosotros").offset().top; 
              //x|alert(">>>"+$("#ancla").height()); 
              timeOut = 2500; 
              htmlbody.animate({ 
                  scrollTop: topScroll
              }, timeOut, 'easeInOutQuint');
            }
            cerrarDelay();
        });
        $(".MenuBeneficios").click(function() {
            if ($("#anclaBeneficio").length > 0){
              topScroll = "";
              timeOut = ""; 
              htmlbody = $('html,body'); 
              valor = 0; 
              topScroll = $("#anclaBeneficio").offset().top; 
              //x|alert(">>>"+$("#ancla").height()); 
              timeOut = 2500; 
              htmlbody.animate({ 
                  scrollTop: topScroll
              }, timeOut, 'easeInOutQuint');
            }
            cerrarDelay();
        });
        $(".MenuTestimonios").click(function() {
            if ($("#anclaTestimonio").length > 0){
              topScroll = "";
              timeOut = ""; 
              htmlbody = $('html,body'); 
              valor = 0; 
              topScroll = $("#anclaTestimonio").offset().top; 
              //x|alert(">>>"+$("#ancla").height()); 
              timeOut = 2500; 
              htmlbody.animate({ 
                  scrollTop: topScroll
              }, timeOut, 'easeInOutQuint');
            }
            cerrarDelay();
        });
        $(".MenuVacantes").click(function() {
            if ($("#anclaVacantes").length > 0){
              topScroll = "";
              timeOut = ""; 
              htmlbody = $('html,body'); 
              valor = 0; 
              topScroll = $("#anclaVacantes").offset().top; 
              //x|alert(">>>"+$("#ancla").height()); 
              timeOut = 2500; 
              htmlbody.animate({ 
                  scrollTop: topScroll
              }, timeOut, 'easeInOutQuint');
            }
            cerrarDelay();
        });
        $(".cerrarAlert").click(function() {
            $(".alert-danger").fadeOut("slow");
        });
    });
    
</script>
@endsection
	
