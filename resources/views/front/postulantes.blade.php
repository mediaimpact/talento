@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="{{asset('css/front/postulantes.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-perfil">
		<div class="container-fluid fcorePerfil text-center">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 text-center tituloVacantes">
					<h1>Postulantes</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid inner-perfil">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 fichaPerfil">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
								  <div class="tab-content">
								    <div role="tabpanel" class="tab-pane active" id="postulantes">
								    	@if (session('status'))
											@if(session('status') == "exito")
												<div class="alert alert-success alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Exito</strong> La postulacion ha sido eliminada.
												</div>
											@else
												<div class="alert alert-danger alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Error</strong> Postulacion no eliminada.
												</div>
											@endif
										@endif
								    	
								    	<div class="row">
								    		<div class="col-lg-12">
												<table class="table">
												  <thead>
												    <tr>
												      <th scope="col">#</th>
												      <th scope="col">Nombres</th>
												      <th scope="col">Puesto</th>
												      <th scope="col">Preguntas</th>
												      <th scope="col">CV</th>
												      <th scope="col">Fecha</th>
												      <!-- <th scope="col"></th> -->
												    </tr>
												  </thead>
												  <tbody>
												  	<?php $n = (10 * $postulantes->currentPage()) - 9;?>
												  	@foreach($postulantes as $postulacion)
												    <tr>
												      <th scope="row">{{ $n }}</th>
												      <td>{{ $postulacion->user->name}}</td>
												      <td>{{ $postulacion->vacante->puesto}}</td>
												      <td>
												      	{{$postulacion->preguntas}} / {{$postulacion->respuestas}}
												      </td>
												      <td><a href="{{asset('/cv/'.$postulacion->user->cv)}}" target="_blank">{{ $postulacion->user->cv }}</a></td>
												      <td>{{ $postulacion->created_at }}</td>
												    </tr>
												    <?php $n++;?>
								    				@endforeach
												  </tbody>
												</table>
											</div>
								    	</div>
								    	<div class="col-md-12 text-center">
								    		{{ $postulantes }}
								    	</div>
								    </div>
								  </div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup">
	 	 		<div class="center-popup" >
	        		<div class="popup">
	        			<img src="{{asset('image/trofeo.png')}}">
	        			<div class="row">
	        				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
	        					<h2>¡Hemos registrado tus datos con éxito!</h2>
	        					<p>Revisa tu correo para confirmar la carga de datos, nos comunicaremos contigo pronto</p>
	        					<a href="{{route('vacantes')}}" id="cerrarPopup">OK</a>
	        				</div>
	        			</div>
	        		</div>
	      		</div>
	    </div>	
		<div class="menuBurger">
			<div class="row">
				<ul>
					<li><a href="javascript:void(0)" class="textoBurger MenuNosotros">Empresa</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuBeneficios">Beneficios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuTestimonios">Testimonios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuVacantes">Vacantes</a></li>
					<li><a href="javascript:void(0)" class="textoBurger">Ir a la web</a></li>
					<li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>
			<a href="javascript:void(0)" class="cerrarBurger">X</a>
		</div>
	</section>
@endsection
@section("js")
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/iFile.js')}}"></script>
	<script src="{{asset('js/perfil.js')}}"></script>
@endsection