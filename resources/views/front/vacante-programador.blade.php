@extends('layouts.app')
@section('css')
<link href="{{asset('css/front/desc-vacantes.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-descVacantes">
		<div class="container-fluid fcoreVacantes">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2 text-center tituloVacantes">
					<h1>Vacantes</h1>
					<p>Requerimos talento humano que busque su desarrollo profesional y el incremento de sus capacidades.</p>
				</div>
			</div>
		</div>
		<div class="container-fluid fondoFiltro">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right">
						<p>Filtro de <span>búsqueda</span></p>
					</div>
					<div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1 col-xs-3 col-xs-offset-1">
						<label>Tipo de empleo</label>
						<select>
							<option value="1">Diseñador gráfico</option>
							<option value="2">Editor audiovisual</option>
							<option value="3">Programador</option>
							<option value="4">Community Manager</option>
						</select>
					</div>
					<div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1 col-xs-3 col-xs-offset-1">
						<label>Ubicación</label>
						<select>
							<option value="1">Surco</option>
							<option value="2">Freelance</option>
						</select>
					</div>
				</div>				
			</div>
		</div>
		<div class="container-fluid inner-vacante">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm 12 col-xs-12">
						<a href="{{route('vacantes')}}"><  Regresar a listado de Vacantes</a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 container-descripcion">
						<div class="container-fluid inner-descripcion">
							<div class="container">
								<h2>Programador</h2>
								<img src="{{asset('image/Ubicacion.png')}}"><span>Surco</span>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fechaPublicacion">
								<div>
									<img src="{{asset('image/calendario.png')}}">
									<p>Fecha de publicación: <span>Lunes 23 de Octubre del 2017</span></p>
								</div>
								<div>
									<img src="{{asset('image/calendario.png')}}">
									<p>Fecha de caducidad: <span>Martes 31 de Octubre del 2017</span></p>
								</div>
							
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid">
								<div class="separador"></div>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 descripcion-vacante">
								<h4>Descripción</h4>
								<p>Una buena idea y un buen diseño necesitan de nuestros amigos programadores para que se pueda apreciar y posicionar de manera eficaz en internet.</p>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 funcion-vacante">
								<h4>Funciones</h4>
								<ul>
									<li>Análisis de sistemas y navegadores.</li>
									<li>Configuración servidores.</li>
									<li>Requerimos talento humano que busque su desarrollo profesional y el incremento de sus capacidades.</li>
									<li>Supervisión de proyectos digitales.</li>
								</ul>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 requisitos-vacante">
								<h4>Requisitos:</h4>
								<ul>
									<li>1 año de experiencia en puestos similares.</li>
									<li>CV actualizado.</li>
									<li>Enviar pretensiones salariales.</li>
								</ul>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 beneficios-vacante">
								<h4>Beneficios:</h4>
								<ul>
									<li>Gratificaciones.</li>
									<li>Vacaciones.</li>
									<li>Buen clima laboral.</li>
									<li>Red de reconocimiento interno.</li>
									<li>Línea de carrera.</li>
									<li>Beneficios de ley.</li>
									<li>Actividades de integración.</li>
									<li>Capacitaciones online.</li>
								</ul>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 horario-vacante">
								<h4>Horario:</h4>
								<p>Lunes a viernes de 9:00 a.m. – 7:00 p.m.</p>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 postular-vacante">
								<!-- <a href="{{route('perfil')}}" id="postular">Postular</a> -->
								<a href="{{route('perfil')}}">Postular</a>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-fluid">
								<div class="separador"></div>
							</div>
						</div>
						<div class="row seccion-descripcion">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 compartir-vacante">
								<p>Compartir en </p>
								<a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a>
								<a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 containerTrelacionados">
						<div class="tituloPrelacionados">
							<h3>Puestos relacionados</h3>
						</div>
						<div class="pRelacionados">
							<span>Publicado: 26/10/17</span>
							<h3>Diseñador Gráfico</h3>
							<h4>Surco</h4>
							<a href="{{route('descripcion')}}">Ver detalles</a>
						</div>
						<div class="pRelacionados">
							<span>Publicado: 26/10/17</span>
							<h3>Community Manager</h3>
							<h4>Surco</h4>
							<a href="{{route('vacante-community-manager')}}">Ver detalles</a>
						</div>
						<div class="pRelacionados">
							<span>Publicado: 26/10/17</span>
							<h3>Editor audiovisual</h3>
							<h4>Surco</h4>
							<a href="{{route('vacante-editor-audiovisual')}}">Ver detalles</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup">
	 	 		<div class="center-popup" >
	        		<div class="popup">
	        			<img src="{{asset('image/imglogin.png')}}">
	        			<div class="row">
	        				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
	        					<form>
	        						<input type="text" placeholder="Usuario" class="usuario">
	        						<input type="text" placeholder="Contraseña" class="contrasena">
	        						<button>Iniciar Sesión</button>
	        					</form>
	        				</div>
	        				<div class="col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3 col-xs-3 col-xs-offset-3">
	        					<input type="checkbox" name="">
	        					<label>Recordar mi contraseña</label>
	        				</div>
	        				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right">
	        					<a href="javascript:void(0)">¿Olvidaste tu contraseña?</a>
	        				</div>
	        			</div>
	        			<div class="separador">
	        				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        					<h4>o</h4>
	        				</div>
	        			</div>
	        			<div class="facebook">
	        				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        					<a href="javascript:void(0)"><i class="icon-facebook"></i> <span>Continue with facebook</span></a>
	        				</div>
	        			</div>
	        			<div class="registrate">
	        				<div class="col-lg-5 col-lg-offset-4 col-md-5 col-md-offset-4 col-sm-5 col-sm-offset-4 col-xs-5 col-xs-offset-4">
	        					<span>¿Aún no tienes usuario?</span>
	        					<a href="{{route('register')}}">Regístrate</a>
	        				</div>
	        			</div>
	          			<a href="javascript:void(0)" id="cerrarPopup">X</a>
	        		</div>
	      		</div>
	    	</div>	
		<!-- <div class="menuBurger">
			<div class="row">
				<ul>
					<li><a href="javascript:void(0)" class="textoBurger MenuNosotros">Empresa</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuBeneficios">Beneficios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuTestimonios">Testimonios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuVacantes">Vacantes</a></li>
					<li><a href="javascript:void(0)" class="textoBurger">Ir a la web</a></li>
					<li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>
			<a href="javascript:void(0)" class="cerrarBurger">X</a>
		</div> -->
	</section>
@endsection
@section("js")
	<script>
    $(document).ready(function(){
        $(".icon-menu").click(function(event) {
            $(".menuBurger").css({"display":"flex"});
            $(".menuBurger").css({"transition":"1s"});
            $(".menuBurger").css({"transform":"translate(0)"});   
            $(".menuBurger").css({"width":"30%"});
            $("body").css({"overflow": "hidden"});
        });
        $(".cerrarBurger").click(function(event) {
            $(".menuBurger").css({"transition":"1s"}); 
            $(".menuBurger").css({"transform":"translate(100%)"});
            $("body").css({"overflow": "initial"});
        });
        $("#cerrarPopup").click(function(event){
            $("#popup").fadeOut("slow");
        });
        $("#postular").click(function(event){
        	$("#popup").fadeIn("slow");
        })
    });
    
</script>
@endsection