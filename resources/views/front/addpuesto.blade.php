@extends('layouts.app')
@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<link href="{{asset('css/front/puestos.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-perfil">
		<div class="container-fluid fcorePerfil text-center">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 text-center tituloVacantes">
					<h1>Nuevo Puestos</h1>
				</div>
			</div>
		</div>
		<div class="container-fluid inner-perfil">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fichaPerfil">
						<div class="row">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
								@include('layouts.menuadm')
							</div>
							<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
								<div class="row">
								  <div class="tab-content">
								    <div role="tabpanel" class="tab-pane active" id="postulantes">
								    	@if (session('status'))
											@if(session('status') == "exito")
												<div class="alert alert-success alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Exito</strong> Nuevo puesto creado.
												</div>
											@else
												<div class="alert alert-danger alert-dismissible fade in" role="alert"> 
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> 
													<strong>Error</strong> Puesto no creado.
												</div>
											@endif
										@endif
								    	
								    	<div class="row">
								    		<form action="{{route('create_puesto')}}" method="POST" id="frm-puesto">
								    			<div class="col-lg-12">
								    				<h2>Crear Nuevo Puesto</h2>
								    				<hr>
								    			</div>
								    			{{ csrf_field() }}
								    			<div class="paso1">
									    			<div class="col-lg-12">
									    				<div class="row">
									    					<div class="col-lg-4">
									    						<label class="radio-inline">
																	<input type="radio" name="tipo" value="1"> Part-Time
																</label>
									    					</div>
									    					<div class="col-lg-4">
									    						<label class="radio-inline">
																	<input type="radio" name="tipo" value="2"> Full-Time
																</label>
									    					</div>
									    					<div class="col-lg-4">
									    						<label class="radio-inline">
																	<input type="radio" name="tipo" value="3"> Administrativo
																</label>
									    					</div>
									    				</div>
									    			</div>
										    		<div class="col-lg-12">
														<div class="form-group">
														    <label for="puesto">Puesto:</label>
														    <input type="text" class="form-control" name="puesto" id="puesto" placeholder="Puesto">
														</div>
														<div class="form-group">
														    <label for="slug">Url Amigable:</label>
														    <input type="text" class="form-control" name="slug" id="slug" placeholder="nuevo-puesto-trabajo">
														    <div id="gurl"></div>
														</div>
														<div class="row">
															<div class="col-lg-6">
																<div class="form-group">
																    <label for="ubicacion">Ubicación:</label>
																    <select  class="form-control" name="ubicacion" id="ubicacion">
																    		<option value=""></option>
																    		@foreach($ubicaciones as $ubicacion)
																    		<option value="{{$ubicacion->id}}">{{$ubicacion->descripcion}}</option>
																    		@endforeach
																	</select>
																</div>
															</div>
															<div class="col-lg-6">
																<div class="form-group">
																    <label for="categoria">Categoría:</label>
																    <select  class="form-control" name="categoria" id="categoria">
																    		<option value=""></option>
																    		@foreach($categorias as $categoria)
																    		<option value="{{$categoria->id}}">{{$categoria->descripcion}}</option>
																    		@endforeach
																	</select>
																</div>
															</div>
														</div>
														
														<div class="form-group">
														    <label for="descripcion">Descripción:</label>
														    <textarea  id="descripcion" class="descripcion editor" name="descripcion" ckeditor="editorOptions" ></textarea>
														</div>

														<div class="form-group">
														    <label for="funciones">Funciones:</label>
														    <textarea  id="funciones" class="funciones editor" name="funciones" ckeditor="editorOptions" ></textarea>
														</div>

														<div class="form-group">
														    <label for="requisitos">Requisitos:</label>
														    <textarea  id="requisitos" class="requisitos editor" name="requisitos" ckeditor="editorOptions" ></textarea>
														</div>

														<div class="form-group">
														    <label for="beneficios">Beneficios:</label>
														    <textarea  id="beneficios" class="beneficios editor" name="beneficios" ckeditor="editorOptions" ></textarea>
														</div>

														<div class="form-group">
														    <label for="horario">Horario:</label>
														    <textarea  id="horario" class="horario editor" name="horario" ckeditor="editorOptions" ></textarea>
														</div>
														<div class="form-group">
														    <label for="horario">Fecha de vencimiento:</label>
														    <input type="text" name="fechav" id="fechav" class="form-control">
														</div>
														<div class="row">
															<div class="col-md-2"></div>
															<div class="col-md-8" id="error"></div>
														</div>
														<div class="row">
															<div class="col-md-3"></div>
															<div class="col-md-6">
																<a href="javascript:void(0)" class="btn btn-rojo next">Siguiente</a>
															</div>
														</div>
													</div>
								    			</div>
												<div class="paso2">
													<div class="col-lg-12">
														<h3>Crear Preguntas:</h3>
														<hr>
													</div>
													<div class="col-lg-12">
														<div class="row">
															<div class="col-lg-6">
																<div class="form-group">
																    <label for="pregunta">Pregunta 1:</label>
																    <textarea class="form-control" name="pregunta1" id="pregunta" placeholder="Pregunta" required></textarea>
																</div>
															</div>
															<div class="col-lg-6">
																<label for="respuesta">Alternativas <span class="muted">(seleccione respuesta correcta)</span></label>
																<div class="row form-group">
																	<div class="col-md-11">
																		<input type="text" class="form-control" name="1res1" placeholder="Alternativa 1" required>
																	</div>
																	<div class="col-md-1">
																		<input type="radio" name="r1" value="1" required>
																	</div>
																</div>

																<div class="row form-group">
																	<div class="col-md-11">
																		<input type="text" class="form-control" name="1res2" placeholder="Alternativa 2" required>
																	</div>
																	<div class="col-md-1">
																		<input type="radio" name="r1" value="2" required>
																	</div>
																</div>

																<div class="row form-group">
																	<div class="col-md-11">
																		<input type="text" class="form-control" name="1res3" placeholder="Alternativa 3" required>
																	</div>
																	<div class="col-md-1">
																		<input type="radio" name="r1" value="3" required>
																	</div>
																</div>
															</div>
														</div>
														<div id="maspreguntas"></div>
														<div class="row">
															<div class="col-md-8"></div>
															<div class="col-md-4">
																<a href="javascript:void(0)" class="btn btn-rojo masitem">Agregar pregunta</a>
																<input type="hidden" id="countitem" name="countitem" value="1">
																<input type="hidden" id="itemc" name="itemc" value="1">
															</div>
														</div>
														<div class="row">
															<div class="col-md-4"></div>
															<div class="col-md-4">
																<button type="submit" class="btn btn-primary">Crear Puesto</button>
																<input type="hidden" id="ajaxpregunta" name="ajaxpregunta" value="">
															</div>
														</div>
													</div>
												</div>
								    		</form>
								    	</div>
								    </div>
								  </div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="popup" class="inner-popup">
 	 		<div class="center-popup" >
        		<div class="popup">
        			<img src="{{asset('image/trofeo.png')}}">
        			<div class="row">
        				<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
        					<h2>¡Hemos registrado tus datos con éxito!</h2>
        					<p>Revisa tu correo para confirmar la carga de datos, nos comunicaremos contigo pronto</p>
        					<a href="{{route('vacantes')}}" id="cerrarPopup">OK</a>
        				</div>
        			</div>
        		</div>
      		</div>
	    </div>	
		<div class="menuBurger">
			<div class="row">
				<ul>
					<li><a href="javascript:void(0)" class="textoBurger MenuNosotros">Empresa</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuBeneficios">Beneficios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuTestimonios">Testimonios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuVacantes">Vacantes</a></li>
					<li><a href="javascript:void(0)" class="textoBurger">Ir a la web</a></li>
					<li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>
			<a href="javascript:void(0)" class="cerrarBurger">X</a>
		</div>
	</section>
@endsection
@section("js")
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/iFile.js')}}"></script>
	<script src="{{asset('js/perfil.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>
	<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    
	<script>
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip();

		  $(".next").click(function(){
		  	var t = $('input:radio[name=tipo]:checked').val();
		  	var p = $('#puesto').val();
		  	var c = $('#categoria').val();
		  	var u = $('#ubicacion').val();
		  	var d = $('#descripcion').val();
		  	var f = $('#funciones').val();
		  	var r = $('#requisitos').val();
		  	var b = $('#beneficios').val();
		  	var h = $('#horario').val();
		  	var s = $('#slug').val();
		  	if(t!="" && p!="" && u!="" && d!="" && f!="" && r!="" && b!="" && h!="" && c!="" && s!=""){
			  	$(".paso1").css('display','none');
			  	$(".paso2").css('display','block');
		  	}
		  	else{
		  		$("#error").html('<div class="alert alert-danger" role="alert">Verificar que los campos esten correctamente completados</div>');
		  		setTimeout(function(){
		  			$("#error > div").remove();
		  		},2000);
		  		console.log("error");
		  	}
		  });

		  $(".masitem").click(function(){
		  	var conut = $("#countitem").val();
		  	var itemc = $("#itemc").val();
		  	conut = parseInt(conut) + 1;
		  	itemc = parseInt(itemc) + 1;
		  	$("#countitem").val(conut);
		  	$("#itemc").val(itemc);
			$("#maspreguntas").append('<div class="row">'+
			  									'<hr>'+
			  									'<a href="javascript:void(0)" class="menositem">X</a>'+
												'<div class="col-lg-6">'+
													'<div class="form-group">'+
													    '<label for="pregunta">Pregunta <span class="itemci">'+itemc+'</span>:</label>'+
													    '<textarea class="form-control gpregunta" name="pregunta'+conut+'"  id="pregunta'+conut+'" placeholder="Pregunta" required></textarea>'+
													'</div>'+
												'</div>'+
												'<div class="col-lg-6">'+
													'<label for="respuesta">Alternativas<span class="muted">(seleccione respuesta correcta)</span></label>'+
													'<div class="row form-group">'+
														'<div class="col-md-11">'+
															'<input type="text" class="form-control" name="'+conut+'res1" placeholder="Alternativa 1" required>'+
														'</div>'+
														'<div class="col-md-1">'+
															'<input type="radio" name="r'+conut+'" value="1" required>'+
														'</div>'+
													'</div>'+

													'<div class="row form-group">'+
														'<div class="col-md-11">'+
															'<input type="text" class="form-control" name="'+conut+'res2" placeholder="Alternativa 2" required>'+
														'</div>'+
														'<div class="col-md-1">'+
															'<input type="radio" name="r'+conut+'" value="2" required>'+
														'</div>'+
													'</div>'+

													'<div class="row form-group">'+
														'<div class="col-md-11">'+
															'<input type="text" class="form-control" name="'+conut+'res3" placeholder="Alternativa 3" required>'+
														'</div>'+
														'<div class="col-md-1">'+
															'<input type="radio" name="r'+conut+'" value="3" required>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>');
			});

		  	$(document).on('click','.menositem',function(){
		  		$(this).parent().remove();
		  		var it = $("#itemc").val();
		  		it = parseInt(it) - 1;
		  		$("#itemc").val(it);
		  		var itemc = 2;
		  		var cb 	= $("#maspreguntas > .row .itemci");
		  		var c;
		  		cb.each(function(){
		  			c = $(this).text(itemc)[0];
		  			itemc = itemc + 1;
		  		});
		  	});

		});
		$('.editor').summernote({
	        placeholder: 'Descripción ...',
	        tabsize: 2,
	        height: 200
      	});
      	$(function () {
	    	var dateToday = new Date(); 
	        $('#fechav').datetimepicker({
	      		format: 'YYYY-MM-DD',
	        	sideBySide: true,
	        	daysOfWeekDisabled:[0,6],
	        	minDate: dateToday
	        });
	    });
	    $.ajaxSetup({
	        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
	    });
	    $( "#slug" ).blur(function() {

	    	var slug = $("#slug").val();
	    	var data = {slug: slug}
	    	$.ajax({
	            type: "POST",
	            url: '/admin/getslug',
	            data: data,
	            success: function( msg ) {
	            	if(msg == "1"){
	            		$("#gurl").html('<div class="text-danger font-italic">Url ya esta registrada, cambie de url</div>')
	            	}
	            	else{
	            		$("#gurl").html('<div class="text-success font-italic">Url disponible</div>')
	            	}
	                console.log(msg);
	            }
	        });
	    });
	</script>
@endsection