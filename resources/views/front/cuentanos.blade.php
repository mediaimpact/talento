@extends('layouts.app')
@section('css')
<link href="{{asset('css/front/cuentanos.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
	<section class="core-cuentanos">
		<div class="container-fluid fcorecuentanos">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 text-center tituloVacantes">
					<h2>Cuéntanos un poco sobre ti</h2>
				</div>
			</div>
		</div>
		<div class="container-fluid fondoBg">
			<div class="container">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8 box-preguntas">
						<img src="{{asset('image/preguntas.png')}}" alt="">
						<form action="{{route('create_postulacion')}}" method="POST" id="cuentanos">
							{{ csrf_field() }}
							<input type="hidden" value="{{ $vacante->id }}" name="vacante_id">
							<?php $i = 1;?>
							@foreach($preguntas as $pregunta)
								<div class="form-group">
								    <label for="">{{ $i }}. {{ $pregunta->descripcion }} </label>
								    <input type="hidden" value="{{ $pregunta->id }}" name="pregunta_id{{ $i }}">
								    <select name="opcion{{$i}}" class="form-control" required>
								    	<option value=""></option>
								    	@foreach($pregunta->respuestas as $opcion)
								    	<option value="{{ $opcion->id }}">{{ $opcion->descripcion }}</option>
								    	@endforeach
								    </select>
								  </div>
								  <?php $i++;?>
							@endforeach
							<input type="hidden" value="{{ $i -1 }}" name="cantpreguntas">
						  <div class="form-group text-center">
						  	<div class="row">
						  		<div class="col-md-2"></div>
							  	<div class="col-md-4">
							  		<a href="javascript:history.back(1)" class="btn  btn-block btn-default">Cancelar</a>
							  	</div>
							  	<div class="col-md-4">
							  		<!-- <a href="javascript:void(0)" id="postular" class="btn btn-block  btn-default">Procesar postulación</a> -->
							  		<button type="submit" id="postular" class="btn btn-block  btn-defaulty">Finalizar</button>
							  	</div>
						  	</div>
						  </div>
						</form>
					</div>
				</div>				
			</div>
		</div>
		@if(session('status'))
		<div id="popup" class="inner-popup">
 	 		<div class="center-popup" >
        		<div class="popup">
        			<img src="{{asset('image/trofeo.png')}}">
        			<div class="row">
        				<div class="col-md-12">
        					<h2>¡Hemos registrado tus datos con éxito!</h2>
        					<p>Revisa tu correo para confirmar la carga de datos, nos comunicaremos contigo pronto</p>
        					<a href="/vacante/{{ $vacante->slug }}" id="cerrarPopup">OK</a>
        				</div>
        			</div>
        		</div>
      		</div>
    	</div>
    	@endif
		<div class="menuBurger">
			<div class="row">
				<ul>
					<li><a href="javascript:void(0)" class="textoBurger MenuNosotros">Empresa</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuBeneficios">Beneficios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuTestimonios">Testimonios</a></li>
					<li><a href="javascript:void(0)" class="textoBurger MenuVacantes">Vacantes</a></li>
					<li><a href="javascript:void(0)" class="textoBurger">Ir a la web</a></li>
					<li><a href="https://www.facebook.com/MediaImpactPeru" target="_blank"><i class="icon-facebook"></i></a><a href="https://www.linkedin.com/company/3356875?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A3356875%2Cidx%3A1-2-2%2CtarId%3A1439494874225%2Ctas%3Amedia%20impact" target="_blank"><i class="icon-linkedin2"></i></a></li>
				</ul>
			</div>
			<a href="javascript:void(0)" class="cerrarBurger">X</a>
		</div>
	</section>
@endsection
@section("js")
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/cuentanos.js')}}"></script>
@endsection