@extends('layouts.app')
@section('css')
<link href="{{asset('css/front/inicio.css')}}" rel="stylesheet">
<link href="{{asset('css/front/registro.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
<section class="headerR">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2 text-center">
                <h2>Registro</h2>
            </div>
        </div>
    </div>
</section>
<section class="registro">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-form">
                    <img src="{{asset('image/imglogin.png')}}">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-lg-12">
                                <input id="nombre" type="text" class="form-control usuario" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombres">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                            <div class="col-lg-12">
                                <input id="apellido" type="text" class="form-control usuario" name="apellido" value="{{ old('apellido') }}" required autofocus placeholder="Apellidos">

                                @if ($errors->has('apellido'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-lg-12">
                                <input id="correo" type="email" class="form-control correo" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-lg-12">
                                <input id="password" type="password" class="form-control contrasena" name="password" required placeholder="Contraseña">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <input id="rpassword" type="password" class="form-control contrasena" name="password_confirmation" required placeholder="Repetir contraseña">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-block btn-primary">
                                    Registrarme
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="separador">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h4>o registrarme con</h4>
                                </div>
                            </div>
                        </div>
                        <div class="form-group facebook">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{asset('login/facebook')}}">
                                    <i class="icon-facebook"></i>
                                    <span>Continue with facebook</span> 
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection           