@extends('layouts.app')
@section('css')
<link href="{{asset('css/front/inicio.css')}}" rel="stylesheet">
<link href="{{asset('css/front/login.css')}}" rel="stylesheet">
<link href="{{asset('css/front/footer.css')}}" rel="stylesheet">
@endsection
@section('content')
<section class="headerL">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2 text-center ">
                <h2>Inicio de Sesión</h2>
            </div>
        </div>
    </div>
</section>
<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-form">
                    <img src="{{asset('image/imglogin.png')}}">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="user" type="text" class="form-control usuario" name="email" value="{{ old('email') }}" required autofocus placeholder="Usuario">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control contrasena" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Iniciar Sesión
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-6">
                                <div class="checkbox">
                                     <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                                    <label>Recordar mi contraseña</label>
                                </div>
                            </div>
                            <div class="col-xs-6 text-right">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    ¿Olvidaste tu contraseña?
                                </a>
                            </div>   
                        </div>
                        <div class="form-group">
                            <div class="separador">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h4>o</h4>
                                </div>
                            </div>
                        </div>
                        <div class="form-group facebook">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="{{asset('login/facebook')}}">
                                    <i class="icon-facebook"></i>
                                    <span>Continue with facebook</span> 
                                </a>
                            </div>
                        </div>
                        <div class="form-group registrate">
                            <div class="col-lg-12 text-center">
                                <span>¿Aún no tienes usuario?</span>
                                <a href="{{route('register')}}">Regístrate</a>
                                </button>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection           