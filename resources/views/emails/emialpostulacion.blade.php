<html>
<head></head>
<body>
<h2>Postulación - Talento</h2>
<p>Acabas de postular a un puesto de trabajo:</p>
<p><b>Puesto: </b> {{ $puesto}}</p>
<p><b>Descripción: </b> {{ $descripcion}}</p>
<p><b>Distrito:</b> {{$ubicacion}}</p>
<p><b>Url:</b> <a href="{{$url}}" target="_blank">{{$url}}</a> </p>
</body>
</html>