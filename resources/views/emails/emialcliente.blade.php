<html>
<head></head>
<body>
<h2>Nueva Postulación - Talento</h2>
<p>Acabán de postular a la siguiente oferta laboral:</p>
<p><b>Nombres: </b> {{ $nombre}} {{ $apellido}}</p>
<p><b>Email: </b> {{ $email}}</p>
<p><b>Teléfono: </b> {{ $cel}}</p>
<p><b>Puesto: </b> {{ $puesto}}</p>
<p><b>Descripción: </b> {{ $descripcion}}</p>
<p><b>Distrito:</b> {{$ubicacion}}</p>
<p><b>Linkedin:</b> <a href="{{$linkedin}}" target="_blank">{{$linkedin}}</a></p>
<p><b>Url:</b> <a href="{{$url}}" target="_blank">{{$url}}</a></p>
</body>
</html>