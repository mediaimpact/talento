<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
})->name('index');

Route::get('/vacantes', 'PageController@vacantes')->name('vacantes');
Route::get('/fulltime', 'PageController@fulltime')->name('fulltime');
Route::get('/parttime', 'PageController@parttime')->name('parttime');
Route::get('/adminis', 'PageController@adminis')->name('adminis');
Route::get('/vacante/{slug}', 'PageController@showvacante');
Route::get('/cuentanos/{slug}', 'PageController@showpreguntas');
Route::post('/create_postulacion','PageController@create_postulacion')->name('create_postulacion');
Route::get('/postulantes', 'PageController@postulantes')->name('postulantes');
Route::get('/detalle/{id}', 'PageController@detalle')->name('detalle');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/perfil', 'PageController@perfil')->name('perfil');
	Route::post('/perfil','PageController@update_perfil')->name('update_perfil');
	Route::get('/historial', 'PageController@historial')->name('historial');
	Route::get('/eliminar/{id}', 'PageController@eliminar')->name('eliminar');


});

Route::get('/admin', 'PageController@adminlogin')->name('admin');

Route::group(['middleware' => 'admin'], function () {

	Route::get('/admin/puestos', 'AdminController@puestos')->name('puestos');
	Route::get('/admin/puestos/{id}', 'AdminController@ordenar')->name('ordenar');
	Route::get('/admin/detalle_puesto/{id}', 'AdminController@detalle_puesto')->name('detalle_puesto');
	Route::get('/admin/edit_puesto/{id}', 'AdminController@edit_puesto')->name('edit_puesto');
	Route::post('/admin/update_puesto', 'AdminController@update_puesto')->name('update_puesto');

	Route::get('/admin/add_puesto', 'AdminController@add_puesto')->name('add_puesto');
	Route::post('/admin/create_puesto', 'AdminController@create_puesto')->name('create_puesto');
	Route::post('/admin/getslug','AdminController@getslug')->name('getslug');
	Route::get('/admin/delete_puesto/{id}','AdminController@delete_puesto')->name('delete_puesto');
	

	Route::get('/admin/postulantes_vacantes','AdminController@postulantes_vacantes')->name('postulantes_vacantes');

	Route::get('/admin/listpostulantes','AdminController@listpostulantes')->name('listpostulantes');
	Route::get('/admin/listpostulantes/{id}', 'AdminController@filtrolist')->name('filtrolist');


	Route::get('/admin/listrezagados','AdminController@listrezagados')->name('listrezagados');
	Route::get('/admin/listrezagados/{id}', 'AdminController@filtrolistr')->name('filtrolistr');

	Route::get('/admin/detalle_postulante/{id}', 'AdminController@detalle_postulante')->name('detalle_postulante');
	Route::get('/admin/delete_postulante/{id}','AdminController@delete_postulante')->name('delete_postulante');

});

// Route::get('/vacantes-descripcion', function () {
//     return view('front.vacantes-descripcion');
// })->name('descripcion');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/searchvacantes','PageController@searchvacantes')->name('searchvacantes');
Route::post('/searchvacantes','PageController@searchvacantes')->name('searchvacantes');
Route::get('/buscarvacantes','PageController@buscarvacantes')->name('buscarvacantes');
Route::post('/buscarvacantes','PageController@buscarvacantes')->name('buscarvacantes');


Auth::routes();

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');