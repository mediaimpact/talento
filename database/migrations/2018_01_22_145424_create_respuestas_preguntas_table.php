<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_preguntas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pregunta_id')->unsigned()->nullable();
            $table->foreign('pregunta_id')->references('id')->on('preguntas');

            $table->integer('respuesta_id')->unsigned()->nullable();
            $table->foreign('respuesta_id')->references('id')->on('respuestas');

            $table->integer('postulacion_id')->unsigned()->nullable();
            $table->foreign('postulacion_id')->references('id')->on('postulaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas_preguntas');
    }
}
