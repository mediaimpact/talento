<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacantes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('puesto')->nullable();
            $table->timestamp('fecha_publicacion')->nullable();
            $table->timestamp('fecha_caducidad')->nullable();
            $table->text('descripcion')->nullable();
            $table->text('funciones')->nullable();
            $table->text('requisitos')->nullable();
            $table->text('beneficios')->nullable();
            $table->text('horario')->nullable();

            $table->integer('ubicacion_id')->unsigned()->nullable();
            $table->foreign('ubicacion_id')->references('id')->on('ubicaciones');

            $table->integer('tipo_id')->unsigned()->nullable();
            $table->foreign('tipo_id')->references('id')->on('tipos_vacantes');

            $table->integer('categoria_id')->unsigned()->nullable();
            $table->foreign('categoria_id')->references('id')->on('categorias_vacantes');

            $table->string('estado')->nullable();
            $table->string('slug')->nullable();

            $table->string('correo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacantes');
    }
}
