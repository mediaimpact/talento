<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoVacante extends Model
{
    //
    protected $table = 'tipos_vacantes';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'descripcion'
    ];

    public function tipo()
    {
        return $this->hasMany('App\Vacante');
    }
}
