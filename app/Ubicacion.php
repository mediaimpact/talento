<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
    //
    protected $table = 'ubicaciones';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'descripcion'
    ];

    public function ubicaciones()
    {
        return $this->hasMany('App\Vacante');
    }

}
