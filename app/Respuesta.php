<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    //
    protected $table = 'respuestas';

    protected $primary = 'id';
    
    protected $fillable = [
    	'descripcion','pregunta_id','estado'
    ];

    public function pregunta()
    {
        return $this->belongsTo('App\Preguntas');
    }
}
