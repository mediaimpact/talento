<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacante extends Model
{
    //
    protected $table = 'vacantes';

    protected $primary = 'id';
    
    protected $fillable = [
    	'puesto','fecha_publicacion','fecha_caducidad','descripcion','funciones','requisitos','beneficios','horario','ubicacion_id','tipo_id','categoria_id','estado' ,'slug','correo'
    ];

    public function categoriavacante()
    {
        return $this->belongsTo('App\CategoriaVacante');
    }

    public function categoria()
    {
        return $this->belongsTo('App\CategoriaVacante');
    }

    public function ubicacion()
    {
        return $this->belongsTo('App\Ubicacion');
    }

    public function tipovacante()
    {
        return $this->belongsTo('App\TipoVacante');
    }

    public function tipo()
    {
        return $this->belongsTo('App\TipoVacante');
    }
}
