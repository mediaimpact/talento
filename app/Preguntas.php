<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntas extends Model
{
    //
    protected $table = 'preguntas';

    protected $primary = 'id';
    
    protected $fillable = [
    	'descripcion','vacante_id'
    ];

    public function vacante()
    {
        return $this->belongsTo('App\Vacante');
    }

    public function respuestas(){
        //return $this->hasmany(RutaAtractivo::class,'id');

        return $this->hasmany('App\Respuesta','pregunta_id');

        //return $this->belongsToMany(Atractivo::class,'rutas_atractivos','idRutas');
        
    }

    public function respuestas_preguntas()
    {
        return $this->hasMany('App\RespuestaPregunta');
    }
}
