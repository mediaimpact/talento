<?php

namespace App\Http\Controllers\Auth;

use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\User;
use URL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\SocialProvider;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected function authenticated(Request $request, $user)
    {
        if(isset($_POST["slug"])){
            return redirect('vacante/'.$_POST["slug"]);
        }
        else{
            if( \Auth::user()->tipo == 1){
                return redirect('/admin/puestos');
            }
            else{
                return redirect('/perfil');
            }
        }
    }
    //protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        if(isset($_GET['slug'])){
            session()->flash('vacante_key', $_GET['slug']);
        }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
         try
         {
            $facebookFields = [
                'first_name', // Default
                'last_name', // Default
                'name', // Default
                'email', // Default
                'gender', // Default
                'birthday', // I've given permission
                'location', // I've given permission
                'locale', // I've given permission
            ];
            $socialUser = Socialite::driver($provider)->fields($facebookFields)->user(); 
         }
         catch(\Exception $e)
         {
             dd($e);
             return redirect('/perfil');
         }
          

         $socialProvider = SocialProvider::where('provider_id',$socialUser->getId())->first();
        //dd($socialUser);
         if(!$socialProvider)
         {
            if($provider == 'facebook'){
                $user = User::firstOrCreate(
                     ['email' => $socialUser->getEmail()],
                     ['name' => $socialUser->user['first_name']],
                     ['apellido' => $socialUser->user['last_name']],
                     ['photo' => $socialUser->getAvatar()]
                );
                $user->photo = $socialUser->getAvatar();
                $user->save();
            }else{

                if(User::where('email',$socialUser->getEmail())->first()){
                    $user = User::where('email',$socialUser->getEmail())->first();
                }else{
                    $user = new User();
                    $user->email = $socialUser->getEmail();
                    $user->name = $socialUser->getName();
                    $user->save();
                }

                
            }
                $user->socialProviders()->create(
                 ['provider_id' => $socialUser->getId(), 'provider' => $provider]
            );
         }
         else
            $user = $socialProvider->user;
            auth()->login($user);

        if(session('vacante_key')){
             $slug = session('vacante_key');
             session()->forget('vacante_key');
             return redirect('vacante/'.$slug);
        }else{
             return redirect('/perfil');
        }
        //return redirect('/');
    }
}
