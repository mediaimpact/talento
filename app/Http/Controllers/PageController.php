<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Vacante;
use App\CategoriaVacante;
use App\TipoVacante;
use App\Ubicacion;
use App\Preguntas;
use App\Respuesta;
use App\Postulaciones;
use App\User;
use App\RespuestaPregunta;
use Session;

use Illuminate\Support\Facades\Mail;
use App\Mail\Emialpostulacion;

use Illuminate\Support\Facades\Hash;

class PageController extends Controller
{
    
    public function adminlogin(){
        return view('front.admin');
    }
    //
    public function vacantes(){
        $vacantes = Vacante::where('estado','=','A')->paginate(10);
        //dd($tickets);
        return view('front.vacantes')
        ->with('vacantes',$vacantes);
    }

     //
    public function postulantes(){
        $postulantes = Postulaciones::orderBy('id','DESC')
            ->paginate(10);
        //dd($postulantes);
        return view('front.postulantes')
        ->with('postulantes',$postulantes);
    }

    //
    public function fulltime(){
        $vacantes = Vacante::where('tipo_id','=',2)
            ->where('estado','=','A')
            ->paginate(10);
        $searchb = "<span>Full-Time</span>";
        Session::flash('status', $searchb);
        //dd($tickets);
        return view('front.vacantes')
        ->with('searchb', $searchb)
        ->with('vacantes',$vacantes);
    }

    public function parttime(){
        $vacantes = Vacante::where('tipo_id','=',1)
            ->where('estado','=','A')
            ->paginate(10);
        $searchb = "<span>Part-Time</span>";
        //dd($tickets);
        Session::flash('status', $searchb);
        return view('front.vacantes')
        ->with('searchb', $searchb)
        ->with('vacantes',$vacantes);
    }

    public function adminis(){
        $vacantes = Vacante::where('tipo_id','=',3)
            ->where('estado','=','A')
            ->paginate(10);
        $searchb = "<span>Administrativo</span>";
        //dd($tickets);
        Session::flash('status', $searchb);
        return view('front.vacantes')
        ->with('searchb', $searchb)
        ->with('vacantes',$vacantes);
    }

    public function perfil(){
        $areas = CategoriaVacante::all();
        $idu = \Auth::user()->id;
        $dato = User::find($idu);
        //dd($tickets);
        return view('front.perfil')
            ->with('dato',$dato)
            ->with('areas',$areas);
    }

    public function update_perfil(Request $request){
        $user = User::find(\Auth::user()->id);
        $user->fill($request->all());
        if($request->file('cv'))
        {
            $file = $request->file('cv');
            $name = 'talento'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/cv/';
            $file->move($path, $name);
            $user->cv = $name;
        }
        if($request->file('photo'))
        {
            $file = $request->file('photo');
            $name = 'photo'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/image/photo/';
            $file->move($path, $name);
            $user->photo = $name;
        }
        if($request->newpassword){
            $newpassword = Hash::make($request->newpassword);
            $user->password = $newpassword;
        }
        $user->save();

        return back()->with('status', 'Datos actualizados');
    }

    public function historial(){
    	//$historial = Postulaciones::paginate(2);
        $idu = \Auth::user()->id;
        $historial = Postulaciones::join('vacantes','postulaciones.vacante_id','=','vacantes.id')
            ->join('ubicaciones','vacantes.ubicacion_id','=','ubicaciones.id')
            ->where('postulaciones.user_id', $idu)
            ->select('vacantes.puesto as puesto','ubicaciones.descripcion as ubicacion','postulaciones.fecha as fecha','postulaciones.id','vacantes.slug')
            ->paginate(10);
    	//dd($tickets);
        return view('front.historial')
        ->with('historial',$historial);
    }

    public function eliminar($id){
        $respuestas = RespuestaPregunta::where('postulacion_id','=',$id);
        $respuestas->delete();
        
        $postulacion = Postulaciones::find($id);

        //$postulacion->delete();
        if($postulacion->delete()){
            Session::flash('status', 'exito');
        }
        else{
            Session::flash('status', 'error');
        }
        return back();
    }

    public function detalle($id){
        
        $postulacion = Postulaciones::find($id);

        // $respuestas = RespuestaPregunta::join('preguntas','respuestas_preguntas.pregunta_id','=','preguntas.id')
        //     ->join('respuestas','respuestas_preguntas.respuesta_id','=','respuestas.id')
        //     ->where('respuestas_preguntas.postulacion_id','=',$postulacion->id)
        //     ->select('respuestas_preguntas.pregunta_id','respuestas_preguntas.respuesta_id','respuestas_preguntas.postulacion_id','preguntas.descripcion as pregunta','respuestas.descripcion as respuesta','respuestas.estado')
        //     ->get();

        return view('front.detalle')
                ->with('postulacion', $postulacion)
                ->with('respuestas', $respuestas);
        
    }

    public function showvacante($slug){
        $vacante = Vacante::where('slug','=', $slug)->first();

        $catvacante = Vacante::where('categoria_id','=', $vacante->categoria_id)
            ->where('id','!=',$vacante->id)
            ->get();
        //comparamos si ya postulo a la vacante
        if(isset(\Auth::user()->id)){
            $idu = \Auth::user()->id;
            $postulo = Postulaciones::where('user_id','=', $idu)
                    ->where('vacante_id','=', $vacante->id)
                    ->first();
        }
        else{
            $postulo = "";
        }

        $completar = 0;
        if(\Auth::user()){
            $datos = User::find(\Auth::user()->id);

            if($datos->cv == null){
                $completar = 1;
            }
        }

        //dd($catvacante);
        return view('front.descripcion-vacante')
                ->with('postulo', $postulo)
                ->with('catvacante', $catvacante)
                ->with('vacante', $vacante)
                ->with('completar', $completar);
    }

    public function showpreguntas($slug){
    	$vacante = Vacante::where('slug','=', $slug)->first();
        $preguntas = Preguntas::where('vacante_id','=',$vacante->id)
            ->with('respuestas')->get();

    	$catvacante = Vacante::where('categoria_id','=', $vacante->categoria_id)->get();
    	//dd($preguntas);
    	return view('front.cuentanos')
    			->with('preguntas', $preguntas)
    			->with('vacante', $vacante);
    }

    public function create_postulacion(Request $request){
    
        $idu = \Auth::user()->id;
        $idv = $request->vacante_id;
        $fechapp = date('Y-m-d');
        $postulacion = Postulaciones::create(['user_id' => $idu, 'vacante_id'=>$idv,'fecha'=>$fechapp, 'estado'=>"A"]);
        $postulacion->save();

        $idp = $postulacion->id;
        //insertamos en el detalle respuestaspreguntas
        for ($i=1; $i <= $request->cantpreguntas ; $i++) { 
            $respuetaP = RespuestaPregunta::create(['pregunta_id' => $request["pregunta_id".$i], 'respuesta_id'=>$request["opcion".$i],'postulacion_id'=>$idp]);
            $respuetaP->save();
        }

        $resp = DB::table('respuestas_preguntas')
            ->join('respuestas','respuestas_preguntas.respuesta_id','=','respuestas.id')
            ->where('respuestas_preguntas.postulacion_id','=',$idp)
            ->select(DB::raw('Count(respuestas_preguntas.id) as preguntas,Sum(respuestas.estado) as respuestas'))
            ->first();

        //dd($respuestas);
        $postulacion = Postulaciones::find($idp);
        $postulacion->preguntas = $resp->preguntas;
        $postulacion->respuestas = $resp->respuestas;
        $postulacion->save();
        //insertamos las preguntas y respuestas acertadas
        
        //obtenemos el puesto postulado - al postulante
        $vacante = Vacante::find($idv);
        $user = User::find(\Auth::user()->id);
        $too = $user->email;
        $clien = $too;
        $title = $vacante->puesto;
        $url = url('/');
        $url .= '/vacante/'.$vacante->slug;

        Mail::send('emails.emialpostulacion', ['puesto'=>$vacante->puesto,'descripcion'=>$vacante->descripcion,'ubicacion'=>$vacante->ubicacion->descripcion,'url'=>$url], function ($message) use ($too,$title,$clien)
        {
            $message->from('empecemos@mediaimpact.pe', 'Postulación - Talento');
            $message->to($too);
            //$message->bcc('genixxavier@gmail.com');
            $message->subject($clien.' - '.$title);

        });

        //enviar al cliente
        $too2 = $vacante->correo;
        $clien2 = "Postulación";

        $cvv = $user->cv;
        Mail::send('emails.emialcliente', ['puesto'=>$vacante->puesto,'descripcion'=>$vacante->descripcion,'ubicacion'=>$vacante->ubicacion->descripcion,'url'=>$url,'email'=>$user->email,'nombre'=>$user->name,'apellido'=>$user->apellido,'cel'=>$user->telefono,'linkedin'=>$user->linkedin], function ($message) use ($too2,$title,$clien2,$cvv)
        {
            $message->from('empecemos@mediaimpact.pe', 'Nueva Postulación - Talento');
            $message->to('empecemos@mediaimpact.pe');
            //$message->to('genixxavier@gmail.com');
            //$message->bcc('gg@mediaimpact.pe');
            if($cvv){
                $message->attach("cv/".$cvv);
            }
            $message->subject($clien2.' - '.$title);

        });

        Session::flash('status', 'exito');
        return back();
    }

    public function buscarvacantes(Request $request){
        $searchb = "";
    	if($request->categoria){
    		$vacantes = Vacante::where('categoria_id','=', $request->categoria)->where('estado','=','A');
    		if($request->distrito){
    			$vacantes = $vacantes->where('ubicacion_id','=', $request->distrito);
    		}
    		if($request->tipo){
    			$vacantes = $vacantes->where('tipo_id','=', $request->tipo);
    		}
    		$vacantes = $vacantes->paginate(10);
    	}

    	else if($request->distrito){
    		$vacantes = Vacante::where('ubicacion_id','=', $request->distrito)->where('estado','=','A');
    		if($request->categoria){
    			$vacantes = $vacantes->where('categoria_id','=', $request->categoria);
    		}
    		if($request->tipo){
    			$vacantes = $vacantes->where('tipo_id','=', $request->tipo);
    		}
    		$vacantes = $vacantes->paginate(10);
    	}
        else if($request->tipo){
            $vacantes = Vacante::where('tipo_id','=', $request->tipo)->where('estado','=','A');
            if($request->categoria){
                $vacantes = $vacantes->where('categoria_id','=', $request->categoria);
            }
            if($request->distrito){
                $vacantes = $vacantes->where('ubicacion_id','=', $request->distrito);
            }
            $vacantes = $vacantes->where('estado','=','A');
            $vacantes = $vacantes->paginate(10);
        }
    	else{
    		 $vacantes = Vacante::paginate(10);
             $searchb = "<span> Todo </span>";
    		//$vacantes = $vacantes->get();
    	}
        
        if($request->categoria){
            $cc = CategoriaVacante::where('id',"=",$request->categoria)->first();
            $searchb .= "<span>".$cc->descripcion."</span> ";
        }
        if($request->distrito){
            $cc = Ubicacion::where('id',"=",$request->distrito)->first();
            $searchb .= "<span>".$cc->descripcion."</span>";
        }
        if($request->tipo){
            $cc = TipoVacante::where('id',"=",$request->tipo)->first();
            $searchb .= "<span>".$cc->descripcion."</span>";
        }
        Session::flash('status', $searchb);
    	return view('front.vacantes')
                ->with('searchb', $searchb)
    			->with('vacantes', $vacantes);
    }

    public function searchvacantes(Request $request){
        $r1 = Vacante::where('puesto','like','%'.$request->search.'%')
            ->where('estado','=','A')
            ->paginate(10);
        $r2 = Vacante::where('descripcion','like','%'.$request->search.'%')
            ->where('estado','=','A')
            ->paginate(10);
        $vacantes = max($r1, $r2);
    	//dd($vacantes);
        $searchb = "<span>".$request->search."</span>";
        Session::flash('status', $searchb);
    	return view('front.vacantes')
            ->with('searchb', $searchb)
    		->with('vacantes', $vacantes);
    }
}
