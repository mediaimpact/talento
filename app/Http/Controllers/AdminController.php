<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Vacante;
use App\CategoriaVacante;
use App\TipoVacante;
use App\Ubicacion;
use App\User;
use App\Preguntas;
use App\Respuesta;
use App\RespuestaPregunta;
use App\Postulaciones;
use Session;

class AdminController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function puestos(){
        $vacantes = Vacante::where('estado','=','A')
            ->paginate(10);
        return view('front.puestos')
        ->with('vacantes',$vacantes);
    }

    public function postulantes_vacantes(){
    	$vacantes = Vacante::where('estado','=','A')
    		->paginate(6);
        return view('front.postulantes_vacantes')
        ->with('vacantes',$vacantes);
    }

    public function ordenar($id){
    	$vacantes = Vacante::where('tipo_id','=',$id)
            ->where('estado','=','A')
    		->paginate(10);
        return view('front.puestos')
        ->with('vacantes',$vacantes);
    }

    public function getslug(){
    	$slug = $_POST['slug'];
    	$vacante = Vacante::where('slug','=',$slug)->count();
    	return $vacante;
    }

    public function detalle_puesto($id){
        $vacante = Vacante::where('id','=', $id)->first();

        $preguntas = Preguntas::where('vacante_id','=',$vacante->id)
            ->with('respuestas')->get();

        $catvacante = Vacante::where('categoria_id','=', $vacante->categoria_id)
            ->where('id','!=',$vacante->id)
            ->get();

        return view('front.detalle_puesto')
                ->with('catvacante', $catvacante)
                ->with('preguntas', $preguntas)
                ->with('vacante', $vacante);
    }

    public function add_puesto(){
        $tipos = TipoVacante::all();
        $categorias = CategoriaVacante::all();
        $ubicaciones = Ubicacion::all();
        return view('front.addpuesto')
        ->with('tipos',$tipos)
        ->with('categorias',$categorias)
        ->with('ubicaciones',$ubicaciones);
    }

    public function edit_puesto($id){
        $vacante = Vacante::where('id','=', $id)->first();

    	$tipos = TipoVacante::all();
    	$categorias = CategoriaVacante::all();
    	$ubicaciones = Ubicacion::all();

        $preguntas = Preguntas::where('vacante_id','=',$id)
            ->with('respuestas')->get();

        return view('front.edit_puesto')
        ->with('vacante',$vacante)
        ->with('tipos',$tipos)
        ->with('categorias',$categorias)
        ->with('preguntas',$preguntas)
        ->with('ubicaciones',$ubicaciones);
    }

    public function create_puesto(Request $request){
    	//primero inserto puesto
    	$puesto = $request->puesto;
    	$ubicacion = $request->ubicacion;
    	$tipo = $request->tipo;
    	$categoria = $request->categoria;
    	$descripcion = $request->descripcion;
    	$funciones = $request->funciones;
    	$requisitos = $request->requisitos;
    	$beneficios = $request->beneficios;
    	$horario = $request->horario;
    	$fechav = $request->fechav;
    	$fechai = date('Y-m-d H:i:s');
    	$slug = $request->slug;
    	$countitem = $request->countitem;

    	$vacante = Vacante::create(['puesto' => $puesto, 'ubicacion_id'=>$ubicacion,'tipo_id'=>$tipo, 'categoria_id'=>$categoria,'descripcion'=>$descripcion, 'funciones'=>$funciones, 'requisitos'=>$requisitos, 'beneficios'=>$beneficios,'horario'=>$horario,'fecha_publicacion'=>$fechai,'fecha_caducidad'=>$fechav, 'estado'=>'A','slug'=>$slug]);
     	$vacante->save();

     	$idv = $vacante->id;

    	//each
    	for ($i=1; $i <= $countitem ; $i++) {
    		//inserto pregunta
    		if($request["pregunta".$i]){
	    		$pregunta = Preguntas::create(['descripcion' => $request["pregunta".$i], 'vacante_id'=>$idv]);
	            $pregunta->save();

	            $idp = $pregunta->id;

	            $res = $request['r'.$i];
	            if($res == 1){ $rp1 = 1;}else{$rp1 = 0;}
	            if($res == 2){ $rp2 = 1;}else{$rp2 = 0;}
	            if($res == 3){ $rp3 = 1;}else{$rp3 = 0;}
				//each
				$r1 = Respuesta::create(['descripcion' => $request[$i."res1"], 'pregunta_id'=>$idp, 'estado'=>$rp1]);
	            $r1->save();

	            $r2 = Respuesta::create(['descripcion' => $request[$i."res2"], 'pregunta_id'=>$idp, 'estado'=>$rp2]);
	            $r2->save();

	            $r3 = Respuesta::create(['descripcion' => $request[$i."res3"], 'pregunta_id'=>$idp, 'estado'=>$rp3]);
	            $r3->save();
    		}
        }
    	//dd($request);
    	Session::flash('status', 'exito');
        return back();
    }

    public function update_puesto(Request $request){
        //primero inserto puesto
        $id = $request->idpuesto;
        $puesto = $request->puesto;
        $ubicacion = $request->ubicacion;
        $tipo = $request->tipo;
        $categoria = $request->categoria;
        $descripcion = $request->descripcion;
        $funciones = $request->funciones;
        $requisitos = $request->requisitos;
        $beneficios = $request->beneficios;
        $horario = $request->horario;
        $fechav = $request->fechav;
        $slug = $request->slug;
        $countitem = $request->countitem;

        $vacante = Vacante::find($id);
        $vacante->puesto = $puesto;
        $vacante->fecha_caducidad = $fechav;
        $vacante->descripcion = $descripcion;
        $vacante->funciones = $funciones;
        $vacante->requisitos = $requisitos;
        $vacante->beneficios = $beneficios;
        $vacante->horario = $horario;
        $vacante->ubicacion_id = $ubicacion;
        $vacante->tipo_id = $tipo;
        $vacante->categoria_id = $categoria;
        $vacante->slug = $slug;
        $vacante->save();

        //each
        for ($i=1; $i <= $countitem ; $i++) {
            //inserto pregunta
            if($request["pregunta".$i]){
                $idp = $request[$i."idp"];

                $pregunta = Preguntas::find($idp);
                $pregunta->descripcion = $request["pregunta".$i];
                $pregunta->save();

                $res = $request['r'.$i];

                if($res == 1){ $rp1 = 1;}else{$rp1 = 0;}
                if($res == 2){ $rp2 = 1;}else{$rp2 = 0;}
                if($res == 3){ $rp3 = 1;}else{$rp3 = 0;}
                //each
                $r1 = Respuesta::find($request[$i."idres1"]);
                $r1->descripcion = $request[$i."res1"];
                $r1->estado = $rp1;
                $r1->save();

                $r2 = Respuesta::find($request[$i."idres2"]);
                $r2->descripcion = $request[$i."res2"];
                $r2->estado = $rp2;
                $r2->save();

                $r3 = Respuesta::find($request[$i."idres3"]);
                $r3->descripcion = $request[$i."res3"];
                $r3->estado = $rp3;
                $r3->save();
            }
        }
        //dd($request);
        Session::flash('status', 'exito');
        return back();
    }


    public function delete_puesto($id){
        $postulacion = Vacante::find($id);
        $postulacion->estado = "E";
        if($postulacion->save()){
            Session::flash('status', 'exito');
        }
        else{
            Session::flash('status', 'error');
        }
        return back();
    }

    public function filtrolist($id){
    	$postulantes = Postulaciones::join('vacantes', 'postulaciones.vacante_id', '=', 'vacantes.id')
            ->select('postulaciones.*')
            ->where('vacantes.id','=',$id)
            ->where('postulaciones.estado','!=','E')
            ->where('postulaciones.respuestas','>=','3')
            ->paginate(10);
        $tipos = TipoVacante::all();
        $vid = $id;
        return view('front.listpostulantes')
            ->with('tipos',$tipos)
	        ->with('vid',$vid)
	        ->with('postulantes',$postulantes);
    }

    public function listpostulantes(){
        $postulantes = Postulaciones::where('estado','!=','E')
            ->where('respuestas','>=','3')
        	->orderBy('id','DESC')
            ->paginate(10);
        $tipos = TipoVacante::all();
        //dd($postulantes);
        return view('front.listpostulantes')
        ->with('tipos',$tipos)
        ->with('postulantes',$postulantes);
    }

    public function filtrolistr($id){
        $postulantes = Postulaciones::join('vacantes', 'postulaciones.vacante_id', '=', 'vacantes.id')
            ->select('postulaciones.*')
            ->where('vacantes.id','=',$id)
            ->where('postulaciones.estado','!=','E')
            ->where('postulaciones.respuestas','<','3')
            ->paginate(10);
        $tipos = TipoVacante::all();
        $vid = $id;
        return view('front.rezagados')
            ->with('tipos',$tipos)
            ->with('vid',$vid)
            ->with('postulantes',$postulantes);
    }

    public function listrezagados(){
        $postulantes = Postulaciones::where('estado','!=','E')
            ->where('respuestas','<','3')
            ->orderBy('id','DESC')
            ->paginate(10);
        $tipos = TipoVacante::all();
        //dd($postulantes);
        return view('front.rezagados')
        ->with('tipos',$tipos)
        ->with('postulantes',$postulantes);
    }

    public function detalle_postulante($id){
    	$postulacion = Postulaciones::where('id','=',$id)->first();
    	$user_id = $postulacion->user_id;

    	$dato = User::where('id','=',$user_id)->first();

        $areas = CategoriaVacante::all();
        //dd($tickets);
        return view('front.detalle_postulante')
            ->with('dato',$dato)
            ->with('postulacion',$postulacion)
            ->with('areas',$areas);
    }

     public function delete_postulante($id){
    	$postulacion = Postulaciones::find($id);
        $postulacion->estado = "E";
        if($postulacion->save()){
            Session::flash('status', 'exito');
        }
        else{
            Session::flash('status', 'error');
        }
        return back();
    }
}
