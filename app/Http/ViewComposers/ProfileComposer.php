<?php namespace App\Http\ViewComposers;
 
use Illuminate\Contracts\View\View;
use App\Ubicacion as Distritos;
use App\CategoriaVacante as Categorias;
use App\TipoVacante as Tipovacante;
 
class ProfileComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $distritos = Distritos::all();
        $categorias = Categorias::all();
        $tipovacante = Tipovacante::all();

        $view->with('distritos', $distritos)
            ->with('tipovacante', $tipovacante)
            ->with('categorias', $categorias);
    }
 
}