<?php

namespace App\Http\Middleware;

use Closure;

class MdusuarioAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario_actual=\Auth::user();
        if($usuario_actual){
            if($usuario_actual->tipo != 1){
                return redirect()->route('index');
            }
            else{
                return $next($request);
            }
        }
        else{
            return redirect()->route('index');
        }
    }
}
