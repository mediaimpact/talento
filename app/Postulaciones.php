<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulaciones extends Model
{
    //
    protected $table = 'postulaciones';

    protected $primary = 'id';
    
    protected $fillable = [
    	'user_id','vacante_id','fecha','estado','preguntas','respuestas'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function vacante()
    {
        return $this->belongsTo('App\Vacante');
    }
}
