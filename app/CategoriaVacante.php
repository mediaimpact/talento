<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaVacante extends Model
{
    //
    protected $table = 'categorias_vacantes';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'descripcion'
    ];

    public function categoria()
    {
        return $this->hasMany('App\Vacante');
    }
}
