<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespuestaPregunta extends Model
{
    //
    protected $table = 'respuestas_preguntas';

    protected $primary = 'id';
    
    protected $fillable = [

    	'pregunta_id','respuesta_id', "postulacion_id"
    ];

    public function pregunta()
    {
        return $this->belongsTo('App\Preguntas');
    }

    public function respuesta()
    {
        return $this->belongsTo('App\Respuesta');
    }

     public function postulaciones()
    {
        return $this->belongsTo('App\postulaciones');
    }


}
